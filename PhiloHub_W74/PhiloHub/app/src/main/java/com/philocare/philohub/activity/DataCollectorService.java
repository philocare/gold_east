package com.philocare.philohub.activity;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import com.philocare.philohub.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.inuker.bluetooth.library.utils.BluetoothUtils.sendBroadcast;
import static com.philocare.philohub.activity.SysUtils.pathConfig;

public class DataCollectorService extends Service {

    public static final int MSG_START_PROCESS = 22;
    public static final int MSG_DATA_SENT = 23;
    //Foreground service
    private PowerManager.WakeLock wakeLock;
    private boolean isServiceStarted;

    // Unique Identification Number for the Notification.
    // We use it on Notification start, and to cancel it.
    private int NOTIFICATION = R.string.local_service_started;

    // Notificação do serviço
    private NotificationManager mNM;

    private static final String SERVICE_CHANNEL_ID = "service";
    public static DataCollector dataCollector;
    private DataPost dataPost;
    private TimerTask timerTask;
    private TimerTask postTimerTask;
    public int timer_counter=0;

    // Custom Binder
    private MyBinder mLocalbinder = new MyBinder();

    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;
    static final int MSG_SET_VALUE = 3;
    static final int ACTION_DISCONNECT = 10;
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    int mValue = 0;
    int mValue2 = 0;

    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_VALUE:
                    mValue = msg.arg1;
                    mValue2 = msg.arg2;

                    if (mValue2 == ACTION_DISCONNECT) {
                        dataCollector.actionDisconnect();
                        // depois de uns segundos, reconecta
                        // Timer de atualização da imagem da bateria
                        TimerTask reconn_timertask = new TimerTask() {
                            public void run() {
                                dataCollector.actionConnect();
                            }
                        };
                        Timer timer = new Timer();
                        timer.schedule(reconn_timertask, 5000);
                    }

                    /*for (int i=mClients.size()-1; i>=0; i--) {
                        try {
                            mClients.get(i).send(Message.obtain(null,
                                    MSG_SET_VALUE, mValue, 0));
                        } catch (RemoteException e) {
                            // The client is dead.  Remove it from the list;
                            // we are going through the list from back to front
                            // so this is safe to do inside the loop.
                            mClients.remove(i);
                        }
                    }*/
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // Getting the instance of Binder
        //Intrinsics.checkParameterIsNotNull(intent, "intent");
        return mMessenger.getBinder();
        //return mLocalbinder;
    }

    //Custom Binder class
    public class MyBinder extends Binder {
        public DataCollectorService getService() {
            return DataCollectorService.this;
        }
    }

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        DataCollectorService getService() {
            return DataCollectorService.this;
        }
    }

    /**
     * Send the operation result logs to the logcat and TextView control on the UI
     *
     * @param string indicating the log string
     * @param  tag activity log tag
     */
    private void logger(String string, String tag) {
        Log.i(tag, string);

        // colocar timestamp
        Date now = new Date();
        string = DateFormat.format("dd/MM/yyyy H:m:s ", now).toString() + string;
        string = string + System.lineSeparator();

        Intent i = new Intent("android.intent.action.MAIN").putExtra(Consts.MESSAGE_TEXT, string);
        this.sendBroadcast(i);

        // salva log em disco
        SysUtils.saveLog(string, tag, Consts.LOG_FILE_NAME_DATA_DATA);
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    public DataCollectorService() {

    }

    /*@Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }*/

    /*@Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }*/

    /**
     * Show a notification while this service is running.
     */
    private void showNotification(String msg) {

        NotificationActivity note;

        note = new NotificationActivity(this, MainActivity.class);
        note.send_note(this.getString(R.string.app_message_title), this.getString(R.string.app_message_title),
                msg,
                R.mipmap.ic_launcher_round);


        /*mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, ReportActivity.class), 0);

        Notification.Builder builder = Build.VERSION.SDK_INT >= 26 ?
                new Notification.Builder((Context)this, Consts.CHANNEL_ID) :
                new Notification.Builder((Context)this);

        CharSequence text = msg;

        Notification notfc = builder.setContentTitle((CharSequence)"Philo Hub").
                setContentText(text).
                setContentIntent(contentIntent).
                setSmallIcon(R.mipmap.ic_launcher).
                setTicker((CharSequence)"Philo Hub").build();*/

        /*
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = msg;

        // The PendingIntent to launch our activity if the user selects this notification
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, ReportActivity.class), 0);

        // Set the info for the views that show in the notification panel.
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_round)  // the status icon
                .setTicker(text)  // the status text
                .setWhen(System.currentTimeMillis())  // the time stamp
                .setContentTitle(getText(R.string.local_service_label))  // the label of the entry
                .setContentText(text)  // the contents of the entry
                .setContentIntent(contentIntent)  // The intent to send when the entry is clicked
                .build();

        // Send the notification.
        mNM.notify(NOTIFICATION, notification);

        return notification;*/
    }

    private Notification createNotification() {
        String notificationChannelId = Consts.CHANNEL_ID;
        if (Build.VERSION.SDK_INT >= 26) {

            Object nmanager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            //Object nmanager = this.getSystemService(NotificationManager.class);
            //Object nmanager = this.getSystemService(Context.NOTIFICATION_SERVICE);
            if (nmanager == null) {
                //throw new TypeCastException("null cannot be cast to non-null type android.app.NotificationManager");
            }

            NotificationManager notificationManager = (NotificationManager)nmanager;
            NotificationChannel nchannel = new NotificationChannel(notificationChannelId,
                    (CharSequence)"Philo Hub Service notifications channel",  NotificationManager.IMPORTANCE_HIGH);
            nchannel.setDescription("Philo Hub Service channel");
            nchannel.setSound(null, null);

            notificationManager.createNotificationChannel(nchannel);
        }

        Intent var3 = new Intent((Context)this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity((Context)this, 0, var3, 0);
        Notification.Builder builder = Build.VERSION.SDK_INT >= 26 ?
                new Notification.Builder((Context)this, notificationChannelId) :
                new Notification.Builder((Context)this);
        Notification notfc = builder.setContentTitle((CharSequence)"Serviço de Monitoramento").
                setContentText((CharSequence)"O serviço de monitoramento Philo Hub está sendo executado.").
                setContentIntent(pendingIntent).
                setSmallIcon(R.mipmap.ic_launcher).
                setSound(null, null).
                setTicker((CharSequence)"Philo Hub").build();
        return notfc;
    }

    public void onTaskRemoved(Intent rootIntent) {
        //Intrinsics.checkParameterIsNotNull(rootIntent, "rootIntent");
        Intent var3 = new Intent(this.getApplicationContext(), DataCollectorService.class);
        var3.setPackage(this.getPackageName());
        PendingIntent restartServicePendingIntent = PendingIntent.getService((Context)this, 1, var3,
                PendingIntent.FLAG_ONE_SHOT /*1073741824*/);
        //Intrinsics.checkExpressionValueIsNotNull(var10000, "PendingIntent.getService…dingIntent.FLAG_ONE_SHOT)");
        this.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        Object var10 = this.getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        if (var10 == null) {
            //throw new TypeCastException("null cannot be cast to non-null type android.app.AlarmManager");
        } else {
            AlarmManager alarmService = (AlarmManager)var10;
            alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + (long)1000, restartServicePendingIntent);
        }
    }

    @Override
    public void onCreate() {
        //Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();
        dataCollector = new DataCollector(null, this);
        dataPost = new DataPost(null, this);

        //mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        // Display a notification about us starting.  We put an icon in the status bar.
        //showNotification();

        /*
        Notificações no status bar
        */
        //createNotificationChannel();

        // inicia a busca e conexão com o W74
        startTimer();

        // Foreground service
        Notification notification = this.createNotification();
        this.startForeground(1, notification);

        //Notification not = showNotification();
        //this.startForeground(1, not);


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        return START_STICKY;
    }

    /**
     Inicializa o timer de extração de dados do Huawei Health
     */
    public void startTimer() {
        //set a new Timer
        /**
         Variáveis para o timer de extração de dados
         */
        Timer timer = new Timer();
        Timer postTimer = new Timer();

        // inicializa o timer de post
        initializePostTimerTask();

        //initialize the TimerTask's job
        initializeTimerTask();

        // Mudar para 10 minuots porque o mínimo do W74 é 5 minutos
        timer.schedule(timerTask, 5000, 10 * 60 * 1000); //
        postTimer.schedule(postTimerTask, 2000, 5 * 60 * 1000); //
    }

    /**
     * Inicializa a tarefa (thread) que faz o serviço de conexão da pulseira
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (timer_counter++));
                try {
                    //HideBars();
                    //showNotification("Serviço de extração executando");

                    dataCollector.actionConnect();

                    //Message msg = Message.obtain(null, MSG_START_PROCESS);
                    //mClients.get(0).send(msg);

                    TimerTask batt_timertask = new TimerTask() {
                        public void run() {
                            // Enviar mensagem quando a bateria estiver arriando
                            //mHandler.obtainMessage(1).sendToTarget();
                            //updateProgressBar((byte) batteryValue);
                        }
                    };
                    Timer timer = new Timer();
                    timer.schedule(batt_timertask, 5000);
                    //actionConnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    private boolean checkWifiOnAndConnected() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connManager .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        // Somente WiFi
        if (wifi.isConnected()){
            return true;
        }

        //if (mobile.isConnected()) {
        //    return true;
            // If Internet connected
        //}
        return false;
    }

    /**
     * Inicializa a tarefa (thread) que faz o serviço de conexão da pulseira
     */
    public void initializePostTimerTask() {
        postTimerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (timer_counter++));
                try {
                    //HideBars();

                    // Verifica se o wifi está ligado
                    //android.net.wifi.WifiManager m = (WifiManager) getSystemService(WIFI_SERVICE);
                    //android.net.wifi.SupplicantState s = m.getConnectionInfo().getSupplicantState();
                    //NetworkInfo.DetailedState state = WifiInfo.getDetailedStateOf(s);
                    if (!checkWifiOnAndConnected()) {
                        logger("Sem comunicação com a internet, sem envio de dados.", "timer post");
                        return;
                    }

                    dataPost.postAndGetJsonWebToken();

                    //Message msg = Message.obtain(null, MSG_DATA_SENT);
                    //mClients.get(0).send(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /*
    Cria o canal de comunicação do aplicativo
    */
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(SERVICE_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public void onDestroy() {
        //Toast.makeText(this, "Serviço de extração parado", Toast.LENGTH_LONG).show();
        //showNotification("Serviço extração finalizado");
        logger("Serviço de extração morto!", "destroy");
        Intent intent = new Intent(this, BcastReceiver.class);
        intent.setAction(Consts.RESURRECT_SERVICE);
        sendBroadcast(intent);
    }
}
