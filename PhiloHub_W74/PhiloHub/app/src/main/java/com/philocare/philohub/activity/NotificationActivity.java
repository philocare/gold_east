package com.philocare.philohub.activity;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import com.philocare.philohub.R;

import java.io.File;

public class NotificationActivity extends Activity {

    private Context ctx;

    public NotificationActivity(Context mCtx, Class classAct){
        this.ctx = mCtx;
    }

    public void send_note(String bannerTxt, String title, String txt, int icon){

        Intent tapIntent = new Intent(ctx, ReportActivity.class);
        tapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //snoozeIntent.setAction(MainActivity.ACTION_SNOOZE);
        //snoozeIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent tapPendingIntent = PendingIntent.getActivity(ctx, 0, tapIntent, 0);

        /*Intent snoozeIntent = new Intent(ctx, BcastReceiver.class);
        snoozeIntent.setAction(ACTION_SNOOZE);
        snoozeIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(ctx, 0, snoozeIntent, 0);*/

        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle("textTitle")
                .setContentText("textContent")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);*/

        // -- Funcionando perfeitamente
        Uri soundUri = Uri.parse(String.valueOf(R.raw.beep));
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx, Consts.CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(bannerTxt)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(txt))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(tapPendingIntent)
                .setAutoCancel(true)
                .setSound(soundUri);

        //File sdcardDir = Environment.getExternalStorageDirectory();
        //Uri path = Uri.parse(String.valueOf(R.raw.beep));
        //Ringtone notification = RingtoneManager.getRingtone(ctx, path);
        //notification.play();

        // com Botão de ação
        /*builder = new NotificationCompat.Builder(ctx, CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(bannerTxt)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(txt))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(tapPendingIntent)
                .setAutoCancel(true)
                .addAction(R.mipmap.ic_launcher_round,
                        getString(R.string.snooze),
                        act);*/

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(ctx);
        //NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(1, builder.build());

    }

}
