package com.philocare.philohub.activity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.inuker.bluetooth.library.Code;
import com.inuker.bluetooth.library.Constants;
import com.inuker.bluetooth.library.model.BleGattProfile;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;
import com.philocare.philohub.adapter.CustomLogAdapter;
import com.veepoo.protocol.VPOperateManager;
import com.veepoo.protocol.listener.base.IABleConnectStatusListener;
import com.veepoo.protocol.listener.base.IABluetoothStateListener;
import com.veepoo.protocol.listener.base.IBleWriteResponse;
import com.veepoo.protocol.listener.base.IConnectResponse;
import com.veepoo.protocol.listener.base.INotifyResponse;
import com.veepoo.protocol.listener.data.IAlarm2DataListListener;
import com.veepoo.protocol.listener.data.IAlarmDataListener;
import com.veepoo.protocol.listener.data.IAllSetDataListener;
import com.veepoo.protocol.listener.data.IBatteryDataListener;
import com.veepoo.protocol.listener.data.ICheckWearDataListener;
import com.veepoo.protocol.listener.data.ICustomSettingDataListener;
import com.veepoo.protocol.listener.data.IDeviceFuctionDataListener;
import com.veepoo.protocol.listener.data.IHeartDataListener;
import com.veepoo.protocol.listener.data.IOriginData3Listener;
import com.veepoo.protocol.listener.data.IOriginProgressListener;
import com.veepoo.protocol.listener.data.IPwdDataListener;
import com.veepoo.protocol.listener.data.ISleepDataListener;
import com.veepoo.protocol.listener.data.ISocialMsgDataListener;
import com.veepoo.protocol.model.datas.AlarmData;
import com.veepoo.protocol.model.datas.AlarmData2;
import com.veepoo.protocol.model.datas.AllSetData;
import com.veepoo.protocol.model.datas.BatteryData;
import com.veepoo.protocol.model.datas.CheckWearData;
import com.veepoo.protocol.model.datas.FunctionDeviceSupportData;
import com.veepoo.protocol.model.datas.FunctionSocailMsgData;
import com.veepoo.protocol.model.datas.HRVOriginData;
import com.veepoo.protocol.model.datas.HeartData;
import com.veepoo.protocol.model.datas.OriginData3;
import com.veepoo.protocol.model.datas.OriginHalfHourData;
import com.veepoo.protocol.model.datas.PwdData;
import com.veepoo.protocol.model.datas.SleepData;
import com.veepoo.protocol.model.datas.Spo2hOriginData;
import com.veepoo.protocol.model.enums.EAllSetType;
import com.veepoo.protocol.model.enums.EFunctionStatus;
import com.veepoo.protocol.model.enums.ESpo2hDataType;
import com.veepoo.protocol.model.settings.Alarm2Setting;
import com.veepoo.protocol.model.settings.AlarmSetting;
import com.veepoo.protocol.model.settings.AllSetSetting;
import com.veepoo.protocol.model.settings.CheckWearSetting;
import com.veepoo.protocol.model.settings.CustomSetting;
import com.veepoo.protocol.model.settings.CustomSettingData;
import com.veepoo.protocol.util.Spo2hOriginUtil;
import com.veepoo.protocol.util.VPLogger;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.inuker.bluetooth.library.utils.BluetoothUtils.sendBroadcast;
import static com.veepoo.protocol.model.enums.EFunctionStatus.SUPPORT;
import static com.veepoo.protocol.model.enums.EFunctionStatus.SUPPORT_CLOSE;

public class DataCollector {

    private final static String TAG = MainActivity.class.getSimpleName();
    private final static String YOUR_APPLICATION = "W74_hub";

    private Context mContext;

    // indica o estado de conexão com a pulseira
    private boolean blConnected = false;

    // Tipo OAD ????
    private boolean mIsOadModel;

    private boolean isNewSportCalc = false;
    private byte mBatteryLevel = 0;
    private int watchDataDay = 3;
    private int contactMsgLength = 0;
    private int allMsgLenght = 4;
    private boolean isSleepPrecision = false;
    private static boolean mConnFlag = false;
    private static boolean reset_date = false;

    // Objeto de protocolo W74
    private VPOperateManager mVpoperateManager;
    private IOriginProgressListener originProgressListener;

    // Código do dispositivo
    private String mDeviceCode;

    private String file_timestamp;

    private int count = 0;
    private int days_to_sync = 0;
    public int timer_counter=0;

    // matriz de execução
    int[][] exec_days = new int[3][2];

    private ISleepDataListener iSleepDataListener;

    private TextView p_log;

    /**
     * Send the operation result logs to the logcat and TextView control on the UI
     *
     * @param string indicating the log string
     * @param  tag activity log tag
     */
    private void logger(String string, String tag) {
        Log.i(tag, string);

        // colocar timestamp
        Date now = new Date();
        string = DateFormat.format("dd/MM/yyyy H:m:s ", now).toString() + string;
        string = string + System.lineSeparator();

        Intent i = new Intent("android.intent.action.MAIN").putExtra(Consts.MESSAGE_TEXT, string);
        mContext.sendBroadcast(i);

        // salva log em disco
        SysUtils.saveLog(string, tag, Consts.LOG_FILE_NAME_DATA_DATA);
    }

    //##########################################################################
    // Classes auxiliares
    /**
     * O status gravado de volta
     */
    class WriteResponse implements IBleWriteResponse {

        @Override
        public void onResponse(int code) {
            Logger.t(TAG).i("write cmd status:" + code);
            //logger("write cmd status:" + code, TAG);
        }
    }

    WriteResponse writeResponse = new WriteResponse();

    // Construtor
    public DataCollector(TextView tvlog, Context ctx) {
        p_log = tvlog;
        mContext = ctx;
        //logger("onSearchStarted", "create");
        mVpoperateManager = mVpoperateManager.getMangerInstance(mContext.getApplicationContext());
        VPLogger.setDebug(false);

    }

    /**
     * Monitor the callback status between Bluetooth and the device
     */
    private final IABluetoothStateListener mBluetoothStateListener = new IABluetoothStateListener() {
        @Override
        public void onBluetoothStateChanged(boolean openOrClosed) {
            Logger.t(TAG).i("open=" + openOrClosed);
            logger("open=" + openOrClosed, TAG);
        }
    };

    /**
     * Bluetooth on or off
     */
    private void registerBluetoothStateListener() {
        mVpoperateManager.registerBluetoothStateListener(mBluetoothStateListener);
    }

    private void initLog() {
        Logger.init(YOUR_APPLICATION)
                .methodCount(0)
                .methodOffset(0)
                .hideThreadInfo()
                .logLevel(LogLevel.FULL)
                .logAdapter(new CustomLogAdapter());
    }

    /**
     * Monitor the callback status da pulseira conectada
     */
    private final IABleConnectStatusListener mBleConnectStatusListener = new IABleConnectStatusListener() {

        @Override
        public void onConnectStatusChanged(String mac, int status) {
            if (status == Constants.STATUS_CONNECTED) {
                Logger.t(TAG).i("STATUS_CONNECTED");
                //logger("Conectado", "status conn");
                blConnected = true;
            } else if (status == Constants.STATUS_DISCONNECTED) {
                Logger.t(TAG).i("STATUS_DISCONNECTED");
                //logger("Desconectado", "status conn");
                blConnected = false;
            }
        }
    };

    private void connectDevice(final String mac, final String deviceName) {

        mVpoperateManager.registerConnectStatusListener(mac, mBleConnectStatusListener);

        mVpoperateManager.connectDevice(mac, deviceName, new IConnectResponse() {

            @Override
            public void connectState(int code, BleGattProfile profile, boolean isoadModel) {
                if (code == Code.REQUEST_SUCCESS) {
                    //Bluetooth and device connection status
                    Logger.t(TAG).i("connection succeeded");
                    //logger("connection succeeded", TAG);
                    Logger.t(TAG).i("Whether it is firmware upgrade mode=" + isoadModel);
                    //logger("Whether it is firmware upgrade mode=" + isoadModel, TAG);
                    mIsOadModel = isoadModel;
                } else {
                    Logger.t(TAG).i("Connection failed");
                    logger("Conexão falhou!", "conn dev");
                    IBleWriteResponse resp = new IBleWriteResponse() {
                        @Override
                        public void onResponse(int i) {
                            Toast.makeText(mContext, "Falha na conexão!",
                                    Toast.LENGTH_LONG).show();
                        }
                    };
                    mVpoperateManager.disconnectWatch(resp);
                }
            }
        }, new INotifyResponse() {
            @Override
            public void notifyState(int state) {
                if (state == Code.REQUEST_SUCCESS) {
                    //Status de conexão do dispositivo e Bluetooth
                    Logger.t(TAG).i("Successful monitoring-can perform other operations");
                    //logger("Successful monitoring-can perform other operations", TAG);

                    //Intent intent = new Intent(mContext, OperaterActivity.class);
                    //intent.putExtra("isoadmodel", mIsOadModel);
                    //intent.putExtra("deviceaddress", mac);
                    logger("Conectado a " + mac, "conn dev");
                    mDeviceCode = mac;
                    authDevice();
                    //startActivity(intent);
                } else {
                    Logger.t(TAG).i("Listen failed, reconnect");
                    logger("Falha na conexão com " + mac, "conn dev");
                }

            }
        });
    }

    @NonNull
    private Alarm2Setting getMultiAlarmSetting() {
        int hour = 16;
        int minute = 33;
        int scene = 1;
        boolean isOpen = true;
        String repestStr = "1000010";
        String unRepeatDdate = "0000-00-00";
        return new Alarm2Setting(hour, minute, repestStr, scene, unRepeatDdate, isOpen);
    }

    /**
     * Envia as configurações básicas para o dispositivo
     */
    private void sendBasicConfig() {

        boolean isHaveMetricSystem = true;
        boolean isMetric = true;
        boolean is24Hour = true;
        boolean isOpenAutoHeartDetect = true;
        boolean isOpenAutoBpDetect = false;
        EFunctionStatus isOpenSportRemain = EFunctionStatus.UNSUPPORT;
        EFunctionStatus isOpenVoiceBpHeart = EFunctionStatus.UNSUPPORT;
        EFunctionStatus isOpenFindPhoneUI = EFunctionStatus.UNSUPPORT;
        EFunctionStatus isOpenStopWatch = EFunctionStatus.UNSUPPORT;
        EFunctionStatus isOpenSpo2hLowRemind = EFunctionStatus.SUPPORT;
        EFunctionStatus isOpenWearDetectSkin = EFunctionStatus.SUPPORT;
        EFunctionStatus isOpenAutoInCall = EFunctionStatus.UNSUPPORT;
        EFunctionStatus isOpenAutoHRV = EFunctionStatus.SUPPORT;
        EFunctionStatus isOpenDisconnectRemind = EFunctionStatus.UNSUPPORT;
        EFunctionStatus isOpenSOS = EFunctionStatus.UNSUPPORT;
        EFunctionStatus isOpenPPG = EFunctionStatus.SUPPORT;

        logger("Envia configurações básicas.", TAG);

        int setting = 0, open = 1;
        AllSetSetting mAlarmSetting = new AllSetSetting(EAllSetType.SPO2H_NIGHT_AUTO_DETECT, 22, 0, 8, 0, setting, open);
        VPOperateManager.getMangerInstance(mContext).settingSpo2hAutoDetect(writeResponse, new IAllSetDataListener() {
            @Override
            public void onAllSetDataChangeListener(AllSetData allSetData) {
                String message = "Automatic blood oxygen detection-open\n" + allSetData.toString();
                Logger.t(TAG).i(message);
                //sendMsg(message, 1);
            }
        }, mAlarmSetting);
        //logger("Activate SPO2", TAG);


        CustomSetting customSetting = new CustomSetting(isHaveMetricSystem, isMetric, is24Hour,
                isOpenAutoHeartDetect, isOpenAutoBpDetect, isOpenSportRemain,
                isOpenVoiceBpHeart, isOpenFindPhoneUI, isOpenStopWatch,
                isOpenSpo2hLowRemind, isOpenWearDetectSkin, isOpenAutoInCall, isOpenAutoHRV,
                isOpenDisconnectRemind, isOpenSOS, isOpenPPG
        );
        //logger("Configure all", TAG);

        customSetting.setIsOpenLongClickLockScreen(SUPPORT_CLOSE);
        VPOperateManager.getMangerInstance(mContext).changeCustomSetting(writeResponse, new ICustomSettingDataListener() {
            @Override
            public void OnSettingDataChange(CustomSettingData customSettingData) {
                String message = "Personalized status-Metric / Inch / Time (12/24) / 5 minute measurement switch (heart rate / blood pressure)-setting:\n" + customSettingData.toString();
                Logger.t(TAG).i(message);
                //sendMsg(message, 1);
            }
        }, customSetting);
        //logger("Custom settings", TAG);

        // Apaga todos alarmes
        List<AlarmSetting> alarmSettingList = new ArrayList<>(3);

        AlarmSetting alarmSetting1 = new AlarmSetting(14, 10, false);
        AlarmSetting alarmSetting2 = new AlarmSetting(15, 20, false);
        AlarmSetting alarmSetting3 = new AlarmSetting(16, 30, false);

        alarmSettingList.add(alarmSetting1);
        alarmSettingList.add(alarmSetting2);
        alarmSettingList.add(alarmSetting3);

        VPOperateManager.getMangerInstance(mContext).settingAlarm(writeResponse, new IAlarmDataListener() {
            @Override
            public void onAlarmDataChangeListener(AlarmData alarmData) {
                String message = "Set alarm:\n" + alarmData.toString();
                Logger.t(TAG).i(message);
                // logger(message, "config");
            }
        }, alarmSettingList);
        //logger("Clear alarms", TAG);
    }

    /**
     * Autentica no dispositivo
     */
    public void authDevice() {

        final int[] deviceNumber = {-1};
        final String[] deviceVersion = new String[1];
        final String[] deviceTestVersion = new String[1];

        boolean is24Hourmodel = false;
        logger("Dispositivo autorizado.", "auth");
        VPOperateManager.getMangerInstance(mContext).confirmDevicePwd(writeResponse, new IPwdDataListener() {
            @Override
            public void onPwdDataChange(PwdData pwdData) {
                String message = "PwdData:\n" + pwdData.toString();
                Logger.t(TAG).i(message);
                // logger(message, TAG);
                //sendMsg(message, 1);

                deviceNumber[0] = pwdData.getDeviceNumber();
                deviceVersion[0] = pwdData.getDeviceVersion();
                deviceTestVersion[0] = pwdData.getDeviceTestVersion();

            }
        }, new IDeviceFuctionDataListener() {
            @Override
            public void onFunctionSupportDataChange(FunctionDeviceSupportData functionSupport) {
                String message = "FunctionDeviceSupportData:\n" + functionSupport.toString();
                Logger.t(TAG).i(message);
                // logger(message, TAG);
                //sendMsg(message, 2);
                EFunctionStatus newCalcSport = functionSupport.getNewCalcSport();
                if (newCalcSport != null && newCalcSport.equals(SUPPORT)) {
                    isNewSportCalc = true;
                } else {
                    isNewSportCalc = false;
                }
                watchDataDay = functionSupport.getWathcDay();
                contactMsgLength = functionSupport.getContactMsgLength();
                allMsgLenght = functionSupport.getAllMsgLength();
                isSleepPrecision = functionSupport.getPrecisionSleep() == SUPPORT;

                // flag indicativo de conecção
                mConnFlag = true;
            }
        }, new ISocialMsgDataListener() {
            @Override
            public void onSocialMsgSupportDataChange(FunctionSocailMsgData socailMsgData) {
                String message = "FunctionSocailMsgData:\n" + socailMsgData.toString();
                Logger.t(TAG).i(message);
                // logger(message, TAG);
                //sendMsg(message, 3);
            }
        }, new ICustomSettingDataListener() {
            @Override
            public void OnSettingDataChange(CustomSettingData customSettingData) {
                String message = "FunctionCustomSettingData:\n" + customSettingData.toString();
                Logger.t(TAG).i(message);
                // logger(message, TAG);
                //sendMsg(message, 4);
            }
        }, "0000", is24Hourmodel); // <== Aqui está a senha de acesso a F21

        // Conectou e autenticou, envia as configurações básicas
        sendBasicConfig();

        // inicia as leituras aqui
        //startHRMeasuring();
        logger("Contectado: Extraindo dados...", "auth");

        // começa a extração de dados
        try {
            startExtractData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /*
    Pega informações da bateria
    */
    public void actionGetBatteryInfo() {
        VPOperateManager.getMangerInstance(mContext).readBattery(writeResponse, new IBatteryDataListener() {
            @Override
            public void onDataChange(BatteryData batteryData) {
                String message = "Battery level:\n" + batteryData.getBatteryLevel() + "\n" + "Power:" + batteryData.getBatteryLevel() * 25 + "%";
                Logger.t(TAG).i(message);
                // Aqui vamos mandar uma mensagem para broadcast
                Intent intent = new Intent(mContext, BcastReceiver.class);
                intent.setAction(Consts.BATTERY);
                mBatteryLevel = (byte)batteryData.getBatteryLevel();
                intent.putExtra(Consts.BATTERY_CHARGE, mBatteryLevel);
                sendBroadcast(intent);
            }
        });
    }

    /*
Desconecta o dispositivo
*/
    //public void btnDisconnectClick(View view) {
    public void actionDisconnect() {
        IBleWriteResponse resp = new IBleWriteResponse() {
            @Override
            public void onResponse(int i) {
                Toast.makeText(mContext, "Desconectado!",
                        Toast.LENGTH_LONG).show();
            }
        };

        mVpoperateManager.disconnectWatch(resp);
        mConnFlag = false;
        //callRemoteDisconnect();
    }

    /*
    Extrai dados
    */
    //public void btnGetHealthData(View view)
    public void startExtractData() throws ParseException {

        // Exibe mensagem que está coletando
        //sendMsg("Coletando dados...", 2);

        // pega informação da bateria
        actionGetBatteryInfo();

        /*
        readOriginData (IBleWriteResponse bleWriteResponse, IOriginProgressListener originDataListener, int watchday)
        Se os dados armazenados pelo relógio forem de 3 dias, leia os dados brutos, um a cada 5 minutos, os dados incluem
        pedômetro, frequência cardíaca, pressão arterial, volume de exercício, a ordem de leitura é hoje-ontem-dia antes de ontem,
        teoricamente um total de 288 dados para um dia


        readOriginDataFromDay (IBleWriteResponse bleWriteResponse, IOriginProgressListener originDataListener, dia interno,
        posição interna, dia de exibição interno)
        Leia os dados originais, este método pode personalizar o dia a ser lido e as primeiras linhas do dia a serem lidas,
        para evitar leituras repetidas, por exemplo, definir [ontem, 150], então a ordem de leitura é [ontem {150} -Acabou ontem - anteontem]

        readOriginDataSingleDay (IBleWriteResponse bleWriteResponse, IOriginProgressListener originDataListener,
        dia interno, posição interna, dia de exibição interno)
        Leia os dados brutos. Este método pode personalizar o dia a ser lido e as primeiras entradas do dia a serem lidas,
        e ler apenas o dia. Por exemplo, se [ontem, 150] estiver definido, a ordem de leitura é [ontem {150} -Acabou ontem]

        Descrição da leitura W74 e data salva:
        A diferença pode não dar um dia em minutos que significa que temos de buscar o dia de ontem e de hoje
        */
        /*
        Deverá ser feito um cálculo onde o dia de extração deverá ser especificado em conjunto com o tempo extraido
         */
        String dateString = "";
        String last_timestamp = SysUtils.getHRSavedDate(mContext);

        if (reset_date) {
            last_timestamp = "";
            reset_date = false;
        }

        last_timestamp = "";

        // tenta pegar a data
        if (!last_timestamp.equals("")) {
            // pega somente a data
            dateString = last_timestamp.substring(last_timestamp.indexOf("[") + 1, last_timestamp.indexOf("]"));
        } else {
            // 3 dias atrás
            Date today = new Date();
            // 3 dias em miliseguntos
            long day_minutes_millis = 3 * (24*(60*(60*1000)));
            // hoja menos 3 dias
            long before_yesterday = today.getTime() - day_minutes_millis;
            Date byesterday = new Date(before_yesterday);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            dateString = dateFormat.format(byesterday);
        }

        // transfoma a date em objeto
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        //dateString = "2020-09-01 15:45:00";
        Date last_date = dateFormat.parse(dateString);

        // calcula a diferença com a data de hoje
        Date today = new Date();

        // verifica a diferença para calcular a data inicial de extração de acordo com as regras do W74
        /*
            Dia 0 = Hoje
            Dia 1 = Ontem
            Dia 2 = Antesdeontem
            W74 tem 3 dias
         */
        // verifica o dia
        long time_diff = today.getTime() - last_date.getTime();
        long minutes_diff = (time_diff/1000)/60;
        long day_diff = (minutes_diff/1440);
        //int day_diff = cal_today.get(Calendar.DAY_OF_MONTH) - cal_last_date.get(Calendar.DAY_OF_MONTH);
        // hoje
        if (day_diff == 0) {
            // tem menos de uma dia que não sincroniza, pega dados de hoje
            days_to_sync = 0;
            // busca dados de hoje
            exec_days[1][0] = 0;
            // dados da hora da última busca até agora
            exec_days[0][1] = 1;
        }
        // ontem
        if (day_diff == 1) {
            days_to_sync = 2;
            // se a diferença é um dia, busca dados de ontem
            exec_days[0][0] = 1;
            exec_days[0][1] = 1;
            // e busca dados de hoje
            exec_days[1][0] = 0;
            exec_days[1][1] = 1;
        }
        // anesdeontem
        if (day_diff >= 2) {
            days_to_sync = 3;
            // se a diferença é dois dias ou mais, busca dados de antesdeontem, ontem e hoje
            exec_days[0][0] = 2;
            // coloca no script
            exec_days[0][1] = 1;
            // busca dados de ontem
            exec_days[1][0] = 1;
            // todos os dados
            exec_days[1][1] = 1;
            // busca dados de hoje
            exec_days[2][0] = 0;
            // todos os dados até agora
            exec_days[2][1] = 1;
        }

        // listener de sono
        iSleepDataListener = new ISleepDataListener() {
            @Override
            public void onSleepDataChange(SleepData sleepData) {
                String message = "Sleep data-return:" + sleepData.toString();
                Logger.t(TAG).i(message);
                //sendMsg(message, 1);

                // pega a última data extraída
                file_timestamp = sleepData.getDate();

                Gson gson = new Gson();
                String json = gson.toJson(message);

                //sendMsg(json, 1);
                try {
                    // salva os dados em arquivo
                    // Os arquivos de sono tem de ser sobreescritos por vão várias informações
                    // em um arquivo
                    SysUtils.writeTxtToFile(mDeviceCode, mBatteryLevel, message, SysUtils.pathConfig,
                            file_timestamp + "_" + SysUtils.dataSleepFileName, false);
                } catch (Exception e) {
                    logger("Erro escrevendo arquivo de sono: " + e, "save file");
                    e.printStackTrace();
                }
            }

            @Override
            public void onSleepProgress(float progress) {
                String message = "Sleep data-reading progress:" + "progress=" + progress;
                Logger.t(TAG).i(message);
            }

            @Override
            public void onSleepProgressDetail(String day, int packagenumber) {
                String message = "Sleep data-reading progress:" + "day=" + day + ",packagenumber=" + packagenumber;
                Logger.t(TAG).i(message);
            }

            @Override
            public void onReadSleepComplete() {
                String message = "Sleep data-end of reading";
                Logger.t(TAG).i(message);
                count++;
                // se tem mais dias para buscar, busca
                if (count < days_to_sync) {
                    // Informa o dia que está sendo extraído
                    logger("Extraindo dia " + count, "ext data");
                    VPOperateManager.getMangerInstance(mContext).readOriginDataSingleDay(writeResponse,
                            originProgressListener, exec_days[count][0], exec_days[count][1], watchDataDay);
                } else {
                    count = 0;
                    // Acabou de extrair todos dados, desconecta
                    actionDisconnect();
                    logger("Dados coletados.", "ext data");

                }
            }
        };

        // listener dos dados
        originProgressListener = new IOriginData3Listener() {
            @Override
            public void onOriginFiveMinuteListDataChange(List<OriginData3> originData3List) {
                String message = "Health data [5 minutes]-return:" + originData3List.size();
//                    for (int i = 0; i < originData3List.size(); i++) {
//                        String s = originData3List.get(i).toString();
//                        Logger.t(TAG).i(s);
//                    }
                // logger(message, TAG);
                //sendMsg(message, 2);

                // pega a última data extraída
                //file_timestamp = originData3List.get(originData3List.size() - 1).getmTime().toString();
                file_timestamp = Long.toString(System.currentTimeMillis());
                // linhas de dados
                /*String[] lines = new String[originData3List.size()];
                for (int i = 0; i < originData3List.size(); i++) {
                    lines[i] = originData3List.get(i).toString();
                }*/

                Gson gson = new Gson();
                String json = gson.toJson(originData3List);

                try {
                    // nesse ponto enviar para o servidor - (dados de 5 em 5 minutos)
                    // salva os dados em arquivo
                    SysUtils.writeTxtToFile(mDeviceCode, mBatteryLevel, json, SysUtils.pathConfig,
                            file_timestamp + "_" + SysUtils.dataHealthFileName5min, true);

                    SysUtils.saveHRLastDate(mContext, file_timestamp);
                } catch (IOException e) {
                    logger("Erro escrevendo arquivo de data: " + e, "save file");
                    e.printStackTrace();
                } catch (Exception e) {
                    logger("Erro escrevendo arquivo 5 min: " + e.getMessage(), "save file");
                    e.printStackTrace();
                }

                //sendMsg(originData3List.get(originData3List.size() - 1).getmTime().toString(), 1);
            }

            @Override
            public void onOriginHalfHourDataChange(OriginHalfHourData originHalfHourData) {
                String message = "Health data [30 minutes]-return:" + originHalfHourData.toString();
                // logger(message, TAG);

                //sendMsg(message, 2);

                // Não precisa porque encontrei os passos e sport no 5 min
                //Gson gson = new Gson();
                //String json = gson.toJson(originHalfHourData);

                // nesse ponto enviar para o servidor - (dados de 30 minutos)
                // Salva dados de 30 minutos
                //SysUtils.writeTxtToFile(mDeviceCode, json, SysUtils.pathConfig,
                //        file_timestamp + "_" + SysUtils.dataHealthFileName30min, true);

            }

            @Override
            public void onOriginHRVOriginListDataChange(List<HRVOriginData> originHrvDataList) {
                String message = "Health Data [HRV] -Return:" + originHrvDataList.size();
//                    for (int i = 0; i < originHrvDataList.size(); i++) {
//                        String s = originHrvDataList.get(i).toString();
//                        Logger.t(TAG).i(s);
//                    }
                // logger(message, TAG);
            }

            @Override
            public void onOriginSpo2OriginListDataChange(List<Spo2hOriginData> originSpo2hDataList) {
                String message = "Health Data [Spo2h] -Return:" + originSpo2hDataList.size();
                Spo2hOriginUtil spo2hOriginUtil = new Spo2hOriginUtil(originSpo2hDataList);
                List<Map<String, Float>> tenMinuteData = spo2hOriginUtil.getTenMinuteData(ESpo2hDataType.TYPE_SLEEP);
                for (int i = 0; i < originSpo2hDataList.size(); i++) {
                    String s = originSpo2hDataList.get(i).toString();
                    Logger.t(TAG).i(s);
                }
                // logger(message, TAG);
            }

            @Override
            public void onReadOriginProgressDetail(int day, String date, int allPackage, int currentPackage) {
                String message = "Health data [5 minutes]-read progress:" + day;
                Logger.t(TAG).i(message);
            }

            @Override
            public void onReadOriginProgress(float progress) {
                String message = "Health data [5 minutes]-read progress:" + progress;
                // logger(message, TAG);
                Intent intent = new Intent(mContext, BcastReceiver.class);
                intent.setAction(Consts.DATA_COLLETCED);
                intent.putExtra(Consts.DATA_PART, exec_days[count][0]);
                //PendingIntent pendingIntent = PendingIntent.getBroadcast(
                //        mContext, 2, intent, 0);
                sendBroadcast(intent);
            }

            @Override
            public void onReadOriginComplete() {
                String message = "Health data-end of reading";
                //btHealthData.setEnabled(true);
                // logger(message, TAG);
                //sendMsg(message, 1);
                // terminou de ler os dados, ativa leitura do sono
                // Uma observação que o dados de sono do dia atual é considerado de ontem
                // Por isso somamos +1 no dia
                VPOperateManager.getMangerInstance(mContext).readSleepDataSingleDay(writeResponse,
                        iSleepDataListener, (exec_days[count][0] + 1), watchDataDay);
            }
        };

        // Informa o dia que está sendo extraído
        logger("Extraindo dia " + count, "ext data");

        // executa a ação de buscar dados
        VPOperateManager.getMangerInstance(mContext).readOriginDataSingleDay(writeResponse,
                originProgressListener, exec_days[count][0], exec_days[count][1], watchDataDay);
    }


    /*
        Conecta com o dispositivo cadastrado
    */
    public void actionConnect() {
        if (!blConnected) {
            //logger("Tentando conectar...", "connect");
            //btConnect.setEnabled(false);
            //btDisconnect.setEnabled(true);
            String sv_device = SysUtils.getSavedDevice(mContext);
            String[] sv_dev_parts = sv_device.split(";");

            //###########################################
            // Essa mensagem aparece na tela
            logger("Tentando conectar ao dispositivo: " + sv_dev_parts[1], "connect");

            connectDevice(sv_dev_parts[1], sv_dev_parts[0]);
            //callRemoteConnect(sv_dev_parts[0], sv_dev_parts[1]);

            //postAndGetJsonWebToken();
        }
    }

    public void resetDate() {
        reset_date = true;
    }

    /*
    Começa a medir batimentos
    */
    public void startHRMeasuring() {
        //tv1.setText("Medição HR");
        VPOperateManager.getMangerInstance(mContext).startDetectHeart(writeResponse, new IHeartDataListener() {

            @Override
            public void onDataChange(HeartData heart) {
                String message = "Pulso: " + heart.toString();
                // logger(message, "HR measuring");
                // logger(message, TAG);
                Logger.t(TAG).i(message);
                //sendMsg(message, 1);
                if (heart.toString().contains("NORMAL")) {
                    //Logger.t(TAG).i("HEART_DETECT_STOP");
                    VPOperateManager.getMangerInstance(mContext).stopDetectHeart(writeResponse);
                }
            }
        });
    }

}
