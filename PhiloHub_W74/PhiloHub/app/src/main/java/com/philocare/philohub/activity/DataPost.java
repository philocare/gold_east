package com.philocare.philohub.activity;

import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.veepoo.protocol.util.VPLogger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import static android.content.Context.WIFI_SERVICE;
import static com.inuker.bluetooth.library.utils.BluetoothUtils.sendBroadcast;

public class DataPost {

    private TextView p_log;
    private Context mContext;

    // Construtor
    public DataPost(TextView tvlog, Context ctx) {
        p_log = tvlog;
        mContext = ctx;
        //logger("Envia dados para o servidor: OK", "create");
    }

    /**
     * Send the operation result logs to the logcat and TextView control on the UI
     *
     * @param string indicating the log string
     * @param  tag activity log tag
     */
    private void logger(String string, String tag) {
        Log.i(tag, string);

        // colocar timestamp
        Date now = new Date();
        string = DateFormat.format("dd/MM/yyyy H:m:s ", now).toString() + string;
        string = string + System.lineSeparator();

        Intent i = new Intent("android.intent.action.MAIN").putExtra(Consts.MESSAGE_TEXT, string);
        mContext.sendBroadcast(i);

        // salva log em disco
        SysUtils.saveLog(string, tag, Consts.LOG_FILE_NAME_DATA_POST);
    }

    /**
     * JSON WEBTOKEN - Somente para teste
     */
    private void jsonWebToken() {
        String compactJws = Jwts.builder().claim("email", "philocare")
                .claim("password", "Tobillo2015")
                .signWith(SignatureAlgorithm.HS256, "secret".getBytes())
                .compact();

        logger(compactJws, "web token");
    }

    /**
     * Envia dados para a servidor na nuvem
     */
    public void postAndGetJsonWebToken() throws InterruptedException {
        //HttpURLConnection conn = null;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {

                // executa três vezes a thread
                for (int i = 0; i < 10; i++) {

                    try {
                        //Your code goes here
                        String response = "";

                        // Dados para autenticação
                        StringBuilder sbParams = new StringBuilder();
                        sbParams.append("{");
                        sbParams.append("\"passwd\"");
                        sbParams.append(":");
                        sbParams.append("\"123456aA\"");
                        sbParams.append(",");
                        sbParams.append("\"email\"");
                        sbParams.append(":");
                        sbParams.append("\"philo\"");
                        sbParams.append("}@");

                        // Dados extraidos da pulseira
                        // Lê um arquivo e envia no post
                        String[] samples = SysUtils.readTxtFile();

                        if (samples == null) {
                            logger("Sem dados para enviar.", "send json");
                            continue;
                        }

                        sbParams.append(samples[1]);
                        if (Consts.DEBUG)
                            logger("Enviando arquivo: " + samples[0], "send");

                        // messagem a ser enviada
                        //logger("JSON: " + sbParams.toString(), "send json");


                        try {
                            //String requestURL = "https://www.philocare.com/engine/devicedatareceive_post.php";
                            String requestURL = "https://philo.solutions/engine/devicedatareceive_post.php";

                            URL url;

                            try {
                                url = new URL(requestURL);

                                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                                conn.setReadTimeout(15000);
                                conn.setConnectTimeout(15000);
                                conn.setRequestMethod("POST");
                                conn.setDoInput(true);
                                conn.setDoOutput(true);


                                OutputStream os = conn.getOutputStream();
                                BufferedWriter writer = new BufferedWriter(
                                        new OutputStreamWriter(os, "UTF-8"));
                                writer.write(sbParams.toString());
                                writer.flush();
                                writer.close();
                                os.close();
                                int responseCode = conn.getResponseCode();

                                if (responseCode == HttpsURLConnection.HTTP_OK) {
                                    String line;
                                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                                    while ((line = br.readLine()) != null) {
                                        response += line;
                                    }
                                } else {
                                    response = "";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            //return response;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String millisInString = dateFormat.format(new Date());

                        //logger(millisInString + ": " + response, "send json");
                        // apaga o arquivo enviado
                        if (!response.contains("not")) {
                            logger("Dados recebidos e processados pelo servidor", "send json");
                            SysUtils.deleteFile(samples[0]);
                            if (Consts.DEBUG)
                                logger("Apagando arquivo: " + samples[0], "send");
                        } else {
                            logger("Arquivo: " + samples[0] + " NÃO pode ser processado.", "send");
                            SysUtils.moveFile(samples[0]);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            }
        });

        thread.start();
        //thread.join();
        //thread.start();

        //##############################################
        Intent intent = new Intent(mContext, BcastReceiver.class);
        intent.setAction(Consts.DATA_SENT);
        sendBroadcast(intent);

    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

}
