package com.philocare.philohub.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.speech.tts.Voice;
import android.util.Log;
import android.widget.TextView;

import java.util.Date;
import java.util.Locale;
import java.util.Set;

import android.speech.tts.TextToSpeech;

public class LocalTextToSpeech {

    private TextView p_log;
    private Context mContext;

    private android.speech.tts.TextToSpeech ttsEngine;
    public static final int INTENT_TEXT_SPEECH_CODE = 9;
    private boolean ttsReady = false;

    // Construtor
    public LocalTextToSpeech(TextView tvlog, Context ctx) {
        p_log = tvlog;
        mContext = ctx;
        logger("Inicia Text-To-Speech", "create");
        ttsEngine = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int initStatus) {
                if (initStatus == TextToSpeech.SUCCESS) {

                    Locale localeBR = new Locale("pt","br");

                    int available = ttsEngine.isLanguageAvailable(localeBR) ;
                    //selectedLocale(available);
                    ttsEngine.setLanguage(localeBR);

                    ttsReady = true;
                } else {
                    logger("Can't initialize TextToSpeech", "TTS");
                }
            }
        }, "com.google.android.tts");
    }

    /**
     * Send the operation result logs to the logcat and TextView control on the UI
     *
     * @param string indicating the log string
     * @param  tag activity log tag
     */
    private void logger(String string, String tag) {
        Log.i(tag, string);
        // colocar timestamp
        Date now = new Date();
        string = now.toString() + string;
        p_log.append(string + System.lineSeparator());
        int offset = p_log.getLineCount() * p_log.getLineHeight();
        if (offset > p_log.getHeight()) {
            p_log.scrollTo(0, offset - p_log.getHeight());
        }
    }


    /*public void ttsResult(int requestCode, int resultCode, MainActivity listener ) {

        if (requestCode == INTENT_TEXT_SPEECH_CODE) {
            if (resultCode == android.speech.tts.TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                //Log.i("", "success, create the TTS instance");
                tts = new android.speech.tts.TextToSpeech(mContext, listener);
            } else {
                //Log.i("", "missing data, install it");
                Intent installIntent = new Intent();
                installIntent.setAction(android.speech.tts.TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                listener.startActivity(installIntent);
            }
        }
    }*/

    public void speak_text(String text) {
        if (ttsReady) {
            ttsEngine.speak(text, TextToSpeech.QUEUE_FLUSH, null);

            /*Set<Voice> vcs = ttsEngine.getVoices();
            for (Voice tmpVoice : vcs) {
                if (tmpVoice.getName().toLowerCase().contains(("pt-br-x-afs-local"))) {
                    ttsEngine.setVoice(tmpVoice);
                    logger(tmpVoice.getName(), "voicename");
                }
            }*/
        }
    }

    private void selectedLocale(int available) {
        switch (available) {

            case android.speech.tts.TextToSpeech.LANG_AVAILABLE:
                logger("Locale suportada, mas não por país ou variante!", "locale");
                break;

            case android.speech.tts.TextToSpeech.LANG_COUNTRY_AVAILABLE:
                logger("Locale suportada pela Localidade, mas não por país ou variante!", "locale");
                break;

            case android.speech.tts.TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE:
                logger("Locale suportada !", "locale");
                break;

            case android.speech.tts.TextToSpeech.LANG_MISSING_DATA:
                logger("Locale com dados faltando !", "locale");
                break;

            case android.speech.tts.TextToSpeech.LANG_NOT_SUPPORTED:
                logger("Locale nao suportada !", "locale");
                break;

            default:
                break;
        }
    }

    public void onPause() {

        if(ttsEngine !=null){
            ttsEngine.stop();
            ttsEngine.shutdown();
        }
    }
}
