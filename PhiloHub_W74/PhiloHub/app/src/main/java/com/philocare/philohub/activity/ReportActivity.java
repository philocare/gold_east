package com.philocare.philohub.activity;

import android.annotation.TargetApi;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.philocare.philohub.R;

import java.util.Timer;
import java.util.TimerTask;

public class ReportActivity extends AppCompatActivity {
    public final static String LOG_TAG = "DevicePolicyAdmin";
    DevicePolicyManager philoappDevicePolicyManager;
    ComponentName philoappDevicePolicyAdmin;
    private CheckBox philoappAdminEnabledCheckbox;
    protected static final int REQUEST_ENABLE = 1;
    protected static final int SET_PASSWORD = 2;

    // Allowlist two apps.
    private static final String KIOSK_PACKAGE = "com.philo.care.philocare";
    private static final String PLAYER_PACKAGE = "com.philo.care.philocare";
    private static final String[] APP_PACKAGES = {KIOSK_PACKAGE, PLAYER_PACKAGE};

    private WebView myWebView;
    public int counter=0;
    private Context ctx;
    public Context getCtx() {
        return ctx;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        Toolbar toolbar = (Toolbar) findViewById(R.id.report_toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed, vai para o principal do aplicativo
                Intent intent = new Intent(ReportActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        ctx = this;

        // Mnatem a tela ligada
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        HideBars();

        myWebView = (WebView) findViewById(R.id.mainWebview);

        // apagar cache do webview
        myWebView.clearCache(true);

        WebViewClientImpl webViewClient = new WebViewClientImpl(this);
        myWebView.setWebViewClient(webViewClient);

        myWebView.setWebChromeClient(new WebChromeClient() {
            //Other methods for your WebChromeClient here, if needed..

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }
        });

        myWebView.loadUrl("https://philo.solutions");
        //myWebView.loadUrl("https://www.philocare.com");
        //myWebView.loadUrl("http://192.168.100.3/");

        //Habilitando o JavaScript
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        /*FloatingActionButton fab = findViewById(R.id.backBtn);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myWebView.goBack();
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });
        FloatingActionButton rel = findViewById(R.id.reloadBtn);
        rel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myWebView.reload();
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });
        FloatingActionButton exit = findViewById(R.id.exitBtn);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //myWebView.loadUrl("https://philocare.com/users/logout.php");
                myWebView.loadUrl("http://192.168.100.3/users/logout.php");
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();
            }
        });*/

        // esconter as barras
        View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            // TODO: The system bars are visible. Make any desired
                            HideBars();
                            // adjustments to your UI, such as showing the action bar or
                            // other navigational controls.
                        } else {
                            // TODO: The system bars are NOT visible. Make any desired
                            // adjustments to your UI, such as hiding the action bar or
                            // other navigational controls.
                        }
                    }
                });

        // não permite as barras
        startTimer();

        // faz recarga de 1 em 1 minuto
        startMinuteTimer();
    }

    private void HideBars() {
        //for higher api versions.
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    private TimerTask timerTask;
    private TimerTask timerMinuteTask;
    long oldTime=0;

    /**
     Inicializa o timer de extração de dados do Huawei Health
     */
    public void startTimer() {
        //set a new Timer
        /**
         Variáveis para o timer de extração de dados
         */
        Timer timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask(ctx);

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); //
    }

    /**
     Inicializa o timer de atualização da tela
     */
    public void startMinuteTimer() {
        /**
         Variáveis para o timer
         */
        Timer mintimer = new Timer();

        //initialize the TimerTask's job
        initializeMinuteTimerTask(ctx);

        //schedule the timer, to wake up every 1 second
        mintimer.schedule(timerMinuteTask, 60000, 60000); //
    }

    /**
     * Inicializa a tarefa (thread) que faz o serviço de extração e calculos
     */
    public void initializeTimerTask(Context ctx) {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (counter++));
                try {
                    HideBars();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * Inicializa a tarefa (thread) que faz a atualização da tela
     */
    public void initializeMinuteTimerTask(Context ctx) {
        timerMinuteTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  "+ (counter++));
                try {
                    myWebView.reload();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    //##############################################################################################
    // Funções para assumir o controle do dispositivo
    @Override
    protected void onResume() {
        super.onResume();
        /*if (isMyDevicePolicyReceiverActive()) {
            philoappAdminEnabledCheckbox.setChecked(true);
        } else {
            philoappAdminEnabledCheckbox.setChecked(false);
        }
        philoappAdminEnabledCheckbox
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            Intent intent = new Intent(
                                    DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                            intent.putExtra(
                                    DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                                    philoappDevicePolicyAdmin);
                            intent.putExtra(
                                    DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                                    getString(R.string.admin_explanation));
                            startActivityForResult(intent, REQUEST_ENABLE);
                        } else {
                            philoappDevicePolicyManager
                                    .removeActiveAdmin(philoappDevicePolicyAdmin);
                        }
                    }
                });*/
    }
}