package com.philocare.philohub.activity;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.Toast;
import com.philocare.philohub.R;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;
import static android.widget.Toast.LENGTH_LONG;
import static com.philocare.philohub.activity.SysUtils.pathConfig;

public class BcastReceiver extends BroadcastReceiver {

    private static final String BOOT_COMPLETED = Intent.ACTION_BOOT_COMPLETED;
    private static final String QUICKBOOT_POWERON = "android.intent.action.QUICKBOOT_POWERON";
    private static final String MY_PACKAGE_REPLACED = Intent.ACTION_MY_PACKAGE_REPLACED;

    private static final String TAG = BcastReceiver.class.getSimpleName();

    //private IRemoteService mService;
    private String pathLog = "/philocare/log/";
    private boolean bSave = true;
    private ArrayList<BleDeviceItem> nearbyItemList;
    private Context mCtx;
    private int sleepcount = 0;
    private boolean bStart = false;

    private static JobScheduler jobScheduler = null;

    /**
     * Notificação broadcast
     */
    private NotificationActivity note;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        final String action = intent.getAction();

        Bundle extras = intent.getExtras();

        assert action != null;

        if (intent != null && intent.getAction() != null) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            switch (intent.getAction()) {
                case BOOT_COMPLETED:
                case QUICKBOOT_POWERON:
                    startLoggerService(context);
                    break;
                case MY_PACKAGE_REPLACED:
                    startLoggerService(context);
                    break;
            }
        }

        // Inicia serviço depois da reinicialização
        if (Objects.equals(intent.getAction(), Intent.ACTION_BOOT_COMPLETED)) {
            Intent var3 = new Intent(context, DataCollectorService.class);
            context.startForegroundService(var3);
            return;
        }

        // Inicia serviço depois da reinicialização
        if (Objects.equals(intent.getAction(), Consts.RESURRECT_SERVICE)) {
            Intent var3 = new Intent(context, DataCollectorService.class);
            // salva log em disco
            SysUtils.saveLog("Tentando ressucutar o serviço", "broadcast", Consts.LOG_FILE_NAME_DATA_DATA);
            context.startForegroundService(var3);
            return;
        }

        //scheduleJob(context);

        mCtx = context;

        // progress
        //tv1 = (TextView) findViewById(R.id.tv1);

        // faz bind com o serviço
        //Intent service_intent = new Intent(IRemoteService.class.getName());
        //service_intent.setClassName("com.sxr.sdk.ble.keepfit.client",
        //        "com.philocare.philohub.BleService");
        //mCtx.bindService(service_intent, mServiceConnection, BIND_AUTO_CREATE);

        // Estou nesse ponto onde transferir todas as rotinas de comunicação para cá mas não tive sucesso
        // em fazer o bind com o serviço
        // Imagino que o bind terá de ser feito na main Activity e chamado pelo alarme de tempos em tempos
        // Fazer mais testes
        assert action != null;

        if (action.equals(Consts.ALARM_WAKE)) {
            try {
                //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                File sdcardDir = Environment.getExternalStorageDirectory();
                Uri path = Uri.parse(sdcardDir.getPath() + pathConfig + "/ding.mp3");
                Ringtone notification = RingtoneManager.getRingtone(mCtx, path);
                //Ringtone r = RingtoneManager.getRingtone(mCtx, notification);
                notification.play();
                Toast.makeText(context, "Acordar com alarme!", LENGTH_LONG).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (action.equals(Consts.DATA_SENT)) {
            try {
                File sdcardDir = Environment.getExternalStorageDirectory();
                //Uri path = Uri.parse(String.valueOf(R.raw.ding));
                //Ringtone notification = RingtoneManager.getRingtone(mCtx, path);
                //notification.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (action.equals(Consts.DATA_COLLETCED)) {
            try {
                //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                //Ringtone r = RingtoneManager.getRingtone(mCtx, notification);
                //r.play();
                assert extras != null;
                byte part = (byte)extras.getInt(Consts.DATA_PART, 0);

                String str_day = "";
                switch (part) {
                    case 0:
                        str_day = mCtx.getString(R.string.today); // "hoje."
                        break;
                    case 1:
                        str_day = mCtx.getString(R.string.yesterday); //"ontem.";
                        break;
                    case 2:
                        str_day = mCtx.getString(R.string.before_yesterday); //"antes-de-ontem.";
                        break;
                    default:
                        str_day = mCtx.getString(R.string.unkown_day); //"dia indefinido.";
                }

                note = new NotificationActivity(mCtx, MainActivity.class);
                note.send_note(mCtx.getString(R.string.app_message_title), mCtx.getString(R.string.app_message_title),
                        mCtx.getString(R.string.app_message_extract) + str_day,
                        R.mipmap.ic_launcher_round);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (action.equals(Consts.BATTERY)) {
            // pega o valor dos extras
            assert extras != null;
            MainActivity.batteryValue = extras.getByte(Consts.BATTERY_CHARGE, (byte) 0);
            //MainActivity.updateProgressBar(value);
        }
        //if (action.equals(MainActivity.ACTION_SNOOZE)) {
        //    Toast.makeText(context, "Ação recebida de mensagem", LENGTH_LONG).show();
        //}
        //throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Start logger service
     * @param context Context
     */
    private void startLoggerService(Context context) {
        //DbAccess.newAutoTrack(context);
        Intent intent = new Intent(context, DataCollectorService.class);
        ContextCompat.startForegroundService(context, intent);
    }


    public static void scheduleJob(Context context) {
        if (jobScheduler == null) {
            jobScheduler = (JobScheduler) context
                    .getSystemService(context.JOB_SCHEDULER_SERVICE);
        }
        ComponentName componentName = new ComponentName(context,
                JobService.class);
        JobInfo jobInfo = new JobInfo.Builder(1, componentName)
        // setOverrideDeadline runs it immediately - you must have at least one constraint
        // https://stackoverflow.com/questions/51064731/firing-jobservice-without-constraints
                .setOverrideDeadline(0)
                .setPersisted(true).build();
        jobScheduler.schedule(jobInfo);
    }

    /**
     * Atualizar progress bar
     */
    public void updateProgressBar(byte value) {
        /*ProgressBar pb;
        pb = (ProgressBar) findViewById(R.id.progressBar);
        pb.setProgress(value);*/

        // Imagem
        ImageView mImageView = null;
        //mImageView = (ImageView) findViewById(R.id.imgBattery);
        // Atualiza a imagem da bateria
        if (value == 0) {
            mImageView.setImageResource(R.drawable.battery0);
        }
        // Atualiza a imagem da bateria
        if (value == 1) {
            mImageView.setImageResource(R.drawable.battery25);
        }
        // Atualiza a imagem da bateria
        if (value == 2) {
            mImageView.setImageResource(R.drawable.battery50);
        }
        // Atualiza a imagem da bateria
        if (value == 3) {
            mImageView.setImageResource(R.drawable.battery75);
        }
        // Atualiza a imagem da bateria
        if (value == 4) {
            mImageView.setImageResource(R.drawable.battery100);
        }
    }

}