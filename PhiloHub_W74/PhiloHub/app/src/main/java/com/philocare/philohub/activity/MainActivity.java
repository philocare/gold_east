package com.philocare.philohub.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Process;
import android.os.RemoteException;
import android.renderscript.ScriptGroup;
import android.renderscript.ScriptGroup.Binding;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.inuker.bluetooth.library.search.SearchResult;
import com.inuker.bluetooth.library.utils.BluetoothUtils;
import com.orhanobut.logger.Logger;
import com.philocare.philohub.R;
import com.philocare.philohub.adapter.BleScanViewAdapter;
import com.philocare.philohub.adapter.OnRecycleViewClickCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.*;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        OnRecycleViewClickCallback, TextToSpeech.OnInitListener, ServiceConnection {

    // Nome do canal de comunicação para mensagens no status bar
    //public static final String CHANNEL_ID = "common";
    public static Byte batteryValue = 0;
    public static boolean flag_settings = false;
    private Messenger mService = null;

    /**
     * Handler of incoming messages from service.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case DataCollectorService.MSG_START_PROCESS:
                    //mCallbackText.setText("Received from service: " + msg.arg1);
                    logger("O processo de captura foi iniciado", "handler_msg");
                    break;
                case DataCollectorService.MSG_DATA_SENT:
                    //mCallbackText.setText("Received from service: " + msg.arg1);
                    logger("Dados enviados ao servidor", "handler_msg");
                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    /**
      * Target we publish for clients to send messages to IncomingHandler.
    */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    private BroadcastReceiver mReceiver;

    //public static DataCollector dataCollector;
    private DataPost dataPost;
    private LocalTextToSpeech ttsLocal = null;

    public int timer_counter=0;

    private TimerTask timerTask;
    private TimerTask postTimerTask;

    private BroadcastReceiver broadcastReceiver = new BcastReceiver();

    // Verifica as permissões
    private ArrayList<String> listPermission;// add all permission in list which you used in manifes.xml

    // Handler para atualizar a bateria
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            // Imagem
            ImageView mImageView;
            mImageView = (ImageView) findViewById(R.id.imgBattery);

            if (batteryValue == 0) {
                mImageView.setImageResource(R.drawable.battery0);
            }
            // Atualiza a imagem da bateria
            if (batteryValue == 1) {
                mImageView.setImageResource(R.drawable.battery25);
            }
            // Atualiza a imagem da bateria
            if (batteryValue == 2) {
                mImageView.setImageResource(R.drawable.battery50);
            }
            // Atualiza a imagem da bateria
            if (batteryValue == 3) {
                mImageView.setImageResource(R.drawable.battery75);
            }
            // Atualiza a imagem da bateria
            if (batteryValue == 4) {
                mImageView.setImageResource(R.drawable.battery100);
            }

        }
    };

    private final static String TAG = MainActivity.class.getSimpleName();
    Context mContext = MainActivity.this;
    private final int REQUEST_CODE = 1;
    List<SearchResult> mListData = new ArrayList<>();
    List<String> mListAddress = new ArrayList<>();
    //SwipeRefreshLayout mSwipeRefreshLayout;
    BleScanViewAdapter bleConnectAdatpter;

    private BluetoothManager mBManager;
    private BluetoothAdapter mBAdapter;
    private BluetoothLeScanner mBScanner;
    final static int MY_PERMISSIONS_REQUEST_BLUETOOTH = 0x55;
    TextView mTitleTextView;

    /**
     * Send the operation result logs to the logcat and TextView control on the UI
     *
     * @param string indicating the log string
     * @param  tag activity log tag
     */
    private TextView tvlog;

    private void logger(String string, String tag) {
        Log.i(tag, string);

        // se tem p_log é visual
        if (tvlog != null) {
            // colocar timestamp
            Date now = new Date();
            string = DateFormat.format("dd/MM/yyyy H:m:s ", now).toString() + string;
            string = string + System.lineSeparator();
            tvlog.append(string);
            int offset = tvlog.getLineCount() * tvlog.getLineHeight();
            if (offset > tvlog.getHeight()) {
                tvlog.scrollTo(0, offset - tvlog.getHeight());
            }
        }

        // salva log em disco
        SysUtils.saveLog(string, tag, Consts.LOG_FILE_NAME_DATA_DATA);
    }

    //#############################################################
    // Binder

    // Don't attempt to unbind from the service unless the client has received some
    // information about the service's state.
    private boolean mShouldUnbind;
    private ServiceConnection connection;
    boolean mIsBound;

    DataCollectorService serviceDataCollector;
    // To invoke the bound service, first make sure that this value
    // is not null.
    //private DataCollectorService mBoundService;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            // This is called when the connection with the service has been
            // established, giving us the service object we can use to
            // interact with the service.  Because we have bound to a explicit
            // service that we know is running in our own process, we can
            // cast its IBinder to a concrete class and directly access it.
            //mBoundService = ((DataCollectorService.LocalBinder)service).getService();
            //serviceDataCollector = ((DataCollectorService.MyBinder)service).getService();
            //serviceCommunitcation.setCallBack(MainActivity.this);

            // Criar mensagens com o serviço
            mService = new Messenger(service);

            // Registra o MainActivity como cliente
            try {
                Message msg = Message.obtain(null,
                        DataCollectorService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);

                // Give it some value as an example.
                //msg = Message.obtain(null,
                //        DataCollectorService.MSG_SET_VALUE, this.hashCode(), 0);
                //mService.send(msg);
            } catch (RemoteException e) {
                // In this case the service has crashed before we could even
                // do anything with it; we can count on soon being
                // disconnected (and then reconnected if it can be restarted)
                // so there is no need to do anything here.
            }
            //mCallbackText.setText("Attached.");
            // Give it some value as an example.

            // Tell the user about this for our demo.
            Toast.makeText(mContext, R.string.local_service_connected,
                    Toast.LENGTH_SHORT).show();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            //mBoundService = null;
            serviceDataCollector = null;
            Toast.makeText(mContext, R.string.local_service_disconnected,
                    Toast.LENGTH_SHORT).show();
        }
    };

    void doBindService() {
        // Attempts to establish a connection with the service.  We use an
        // explicit class name because we want a specific service
        // implementation that we know will be running in our own process
        // (and thus won't be supporting component replacement by other
        // applications).
        if (bindService(new Intent(mContext, DataCollectorService.class),
                mConnection, Context.BIND_AUTO_CREATE)) {
            mShouldUnbind = true;
            mIsBound = true;
        } else {
            Log.e("MY_APP_TAG", "Error: The requested service doesn't " +
                    "exist, or this client isn't allowed access to it.");
        }
    }

    void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            DataCollectorService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }

            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
            //mCallbackText.setText("Unbinding.");
        }

        /*    if (mShouldUnbind) {
            // Release information about the service's state.
            unbindService(mConnection);
            mShouldUnbind = false;
        }*/
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Notificação broadcast
     */
    private NotificationActivity note;

    private boolean isPermissionsGranted(ArrayList<String> permissions) {

        // lista de permissões requeridas
        for (String s : permissions) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (checkSelfPermission(s) == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG,"Permissão concedida: " + s);
                } else {
                    Log.v(TAG,"Permissão negada: " + s);
                    ActivityCompat.requestPermissions(this, new String[]{s}, 1);
                    return false;
                }
            }
            else { //permission is automatically granted on sdk<23 upon installation
                Log.v(TAG,"Permissão concedida: " + s);
                return true;
            }
        }
        return true;
    }

    private boolean isPermissionGranted(String permission) {
        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            Log.v(TAG, "Permissão concedida: " + permission);
            return true;
        } else {
            Log.v(TAG, "Permissão negada: " + permission);
            ActivityCompat.requestPermissions(this, new String[]{permission}, 1);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case 1:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                    listPermission.remove(0);
                }  else {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                    for (final String s : permissions) {
                        new AlertDialog.Builder(mContext)
                                .setTitle("Permissão negada para " + s)
                                .setMessage("Negar a permissão para o aplicaivo Philo Hub implicará no seu mal funcionamento. Tem certeza que quer fazer isso?")

                                // Specifying a listener allows you to take an action before dismissing the dialog.
                                // The dialog is automatically dismissed when a dialog button is clicked.
                                .setPositiveButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Pergunta novamente pela permissão
                                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{s}, 1);
                                    }
                                })

                                // A null listener allows the button to dismiss the dialog and take no further action.
                                .setNegativeButton(android.R.string.yes, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }
                return;
        }
        // Other 'case' lines to check for other
        // permissions this app might request.
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listPermission = new ArrayList<String>();
        listPermission.add(WRITE_EXTERNAL_STORAGE);
        listPermission.add(READ_EXTERNAL_STORAGE);
        //listPermission.add(SEND_SMS);
        listPermission.add(BLUETOOTH);
        listPermission.add(BLUETOOTH_ADMIN);
        listPermission.add(RECEIVE_SMS);
        listPermission.add(READ_SMS);
        listPermission.add(RECEIVE_BOOT_COMPLETED);
        listPermission.add(ACCESS_NETWORK_STATE);
        listPermission.add(CHANGE_WIFI_STATE);
        listPermission.add(READ_PHONE_STATE);
        listPermission.add(VIBRATE);
        listPermission.add(INTERNET);
        listPermission.add(ACCESS_COARSE_LOCATION);
        listPermission.add(ACCESS_FINE_LOCATION);
        listPermission.add(WAKE_LOCK);
        listPermission.add(FOREGROUND_SERVICE);
        listPermission.add(REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);

        // Timer de atualização da imagem da bateria
        TimerTask perm_timertask = new TimerTask() {
            public void run() {
                if (listPermission.size() > 0) {
                    //logger("Verificando permissão: " + listPermission.get(0), "perm");
                    if (isPermissionGranted(listPermission.get(0))) {
                        listPermission.remove(0);
                    };
                }
            }
        };
        Timer perm_timer = new Timer();
        perm_timer.schedule(perm_timertask, 0, 1000);

        //isPermissionsGranted(listPermission);
        /*isPermissionGranted(WRITE_EXTERNAL_STORAGE);
        isPermissionGranted(SEND_SMS);
        isPermissionGranted(BLUETOOTH);
        isPermissionGranted(BLUETOOTH_ADMIN);
        isPermissionGranted(RECEIVE_SMS);
        isPermissionGranted(READ_SMS);
        isPermissionGranted(RECEIVE_BOOT_COMPLETED);
        isPermissionGranted(ACCESS_NETWORK_STATE);
        isPermissionGranted(CHANGE_WIFI_STATE);
        isPermissionGranted(READ_PHONE_STATE);
        isPermissionGranted(VIBRATE);
        isPermissionGranted(INTERNET);
        isPermissionGranted(ACCESS_COARSE_LOCATION);
        isPermissionGranted(ACCESS_FINE_LOCATION);
        isPermissionGranted(WAKE_LOCK);
        isPermissionGranted(READ_EXTERNAL_STORAGE);
        isPermissionGranted(FOREGROUND_SERVICE);*/

        //isStoragePermissionGranted();

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        tvlog = findViewById(R.id.data_controller_log_info);

        tvlog.setMovementMethod(new ScrollingMovementMethod());

        //dataCollector = new DataCollector(tvlog, MainActivity.this);

        //dataPost = new DataPost(tvlog, MainActivity.this);

        // verifica se tem algum dispositivo cadastrado
        String sv_device = SysUtils.getSavedDevice(mContext);
        String[] sv_dev_parts = sv_device.split(";");

        if (sv_dev_parts[0].equals("W74")) {

            // verifica se o serviço está rodando
            if (!isMyServiceRunning(DataCollectorService.class)) {
                // Inicia o serviço
                Intent i = new Intent(mContext, DataCollectorService.class);
                //bindService(i, connection, Context.BIND_AUTO_CREATE); // Context.BIND_AUTO_CREATE
                // potentially add data to the intent
                i.putExtra("INIT", "Inicia o serviço");
                mContext.startForegroundService(i);
            } else {
                logger("Serviço já estava rodando.", "create");
            }

            //logger("Conecta com o serviço.", "create");
            doBindService();

            //ttsLocal = new LocalTextToSpeech(tvlog, MainActivity.this);

            // prepara a tela de scaneamento inicial
            initRecyleView();

            // verifica as permissões do usuário android
            //checkPermission();

            // registra um listener para o status de bluetooth pois ele sempre te de estar ligado para
            // que o aplicativo funcione
            //registerBluetoothStateListener();

            // Notificações no status bar

            //createNotificationChannel();

            // Timer de atualização da imagem da bateria
            TimerTask batt_timertask = new TimerTask() {
                public void run() {
                    mHandler.obtainMessage(1).sendToTarget();
                    //updateProgressBar((byte) batteryValue);
                }
            };
            Timer timer = new Timer();
            timer.schedule(batt_timertask, 5000, 15 * 60 * 1000);
        } else {
            logger("Não existe dispositivo cadastrado.", "oncreate");
        }

    } // Fim onCreate

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logger("Interface destruída", "destroy");
        //unregisterReceiver(broadcastReceiver);
        doUnbindService();
    }

    //#############################################################################################
    // AÇÕES DE BOTÕES
    //#############################################################################################

    /**
     * ###################################################################################
     * Captura dados da pulseira
     * ###################################################################################
     */

    public void getDeviceData(View view) {
        //dataCollector.actionConnect();
    }

    public void sendPost(View view) throws InterruptedException {
        dataPost.postAndGetJsonWebToken();
    }

    /*
    Ativa a tela de configuração
     */
    public void btnSettingsClick(View view) {
        Intent settings_intent = new Intent(
                this, SettingsActivity.class);
        startActivity(settings_intent);
    }

    public void btnRestartClick(View view) {
        // desconecta o serviço
        //mShouldUnbind = true;
        //doUnbindService();
        logger("Reinicia o sistema de conexão com o dispositivo", "main_activity");

        try {
            Message msg = Message.obtain(null,
                    DataCollectorService.MSG_SET_VALUE);
            msg.replyTo = mMessenger;

            // Give it some value as an example.
            msg = Message.obtain(null,
                    DataCollectorService.MSG_SET_VALUE, this.hashCode(), DataCollectorService.ACTION_DISCONNECT);
            mService.send(msg);
        } catch (RemoteException e) {
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.
        }
    }
    /*
    Envia um broadcast para o receiver
    */
    public void btnBroadcastClick(View view) {
        Intent intent = new Intent(this, BcastReceiver.class);
        intent.setAction(Consts.ALARM_WAKE);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this.getApplicationContext(), 2, intent, 0);

        note = new NotificationActivity(this, MainActivity.class);
        note.send_note("Olá", "Philo Care Hub", "Estamos monitorando sua saúde!",
                R.mipmap.ic_launcher_round);


        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                + (15 * 1000), pendingIntent);
        Toast.makeText(this, "Alarm set in " + 15 + " seconds",
                Toast.LENGTH_LONG).show();

        //String toSpeak = "Boa apresentação!";
        //ttsLocal.speak_text(toSpeak);
    }

    public void btnReportClick(View view) {
        Intent settings_intent = new Intent(
                this, ReportActivity.class);
        startActivity(settings_intent);
        //String toSpeak = "Boa apresentação!";
        //ttsLocal.speak_text(toSpeak);
    }

    //#############################################################################################
    // AÇÕES DO SISTEMA
    //#############################################################################################

    /*
    Cria o canal de comunicação do aplicativo
     */
    /*private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }*/

    private void initRecyleView() {
        //mSwipeRefreshLayout = (SwipeRefreshLayout) super.findViewById(R.id.mian_swipeRefreshLayout);
        //mRecyclerView = (RecyclerView) super.findViewById(R.id.main_recylerlist);
        mTitleTextView = (TextView) super.findViewById(R.id.main_title);

        //mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        bleConnectAdatpter = new BleScanViewAdapter(this, mListData);
        //mRecyclerView.setAdapter(bleConnectAdatpter);
        //mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL_LIST));
        bleConnectAdatpter.setBleItemOnclick(this);
        //mSwipeRefreshLayout.setOnRefreshListener(this);

        mTitleTextView.setText(getString(R.string.app_name) + " " + getAppVersion(mContext));

        //mInfoTextView = (TextView) super.findViewById(R.id.textViewInfo);

    }

    @Override
    public void onInit(int status) {
        //ttsLocal.initTextToSpeech(status);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        //unregister our receiver
        this.unregisterReceiver(this.mReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //extract our message from intent
                String msg_for_me = intent.getStringExtra(Consts.MESSAGE_TEXT);
                tvlog.append(msg_for_me);
                int offset = tvlog.getLineCount() * tvlog.getLineHeight();
                if (offset > tvlog.getHeight()) {
                    tvlog.scrollTo(0, offset - tvlog.getHeight());
                }
                //log our message value
                Log.i("Main Broadcast recebido", msg_for_me);

            }
        };
        //registering our receiver
        this.registerReceiver(mReceiver, intentFilter);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (BluetoothUtils.isBluetoothEnabled()) {
                //scanDevice();
            } else {
                refreshStop();
            }
        }
    }

    // Refresh do swipe to refresh, ativa o scaneamento de pulseiras  próximas
    @Override
    public void onRefresh() {
        Logger.t(TAG).i("onRefresh");
        if (checkBLE()) {
            //scanDevice();
        }
    }

    private void initBLE() {
        mBManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if (null != mBManager) {
            mBAdapter = mBManager.getAdapter();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBScanner = mBAdapter.getBluetoothLeScanner();
        }
        checkBLE();

    }

    /**
     * Check if the Bluetooth device is turned on
     *
     * @return
     */
    private boolean checkBLE() {
        if (!BluetoothUtils.isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }

    /**
     * End refresh
     */
    void refreshStop() {
        Logger.t(TAG).i("refreshComlete");
        //if (mSwipeRefreshLayout.isRefreshing()) {
        //    mSwipeRefreshLayout.setRefreshing(false);
        //}
    }

    @Override
    public void OnRecycleViewClick(int position) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(mContext, "Connecting, please wait...", Toast.LENGTH_SHORT).show();
            }
        });
        SearchResult searchResult = mListData.get(position);
        //connectDevice(searchResult.getAddress(), searchResult.getName());
    }

    public String getAppVersion(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            String version = info.versionName;
            return version;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        serviceDataCollector = ((DataCollectorService.MyBinder)service).getService();
        logger("Seriço conectado", "main_activity");
        //serviceDataCollector.setCallBack(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        logger("Seriço desconectado", "main_activity");
    }

    @Override
    public void onBindingDied(ComponentName name) {
        logger("Bind finalizado", "main_activity");
    }
}
