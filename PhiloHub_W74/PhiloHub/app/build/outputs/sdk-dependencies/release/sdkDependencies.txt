# List of SDK dependencies of this app, this information is also included in an encrypted form in the APK.
# For more information visit: https://d.android.com/r/tools/dependency-metadata

library {
  maven_library {
    groupId: "com.android.support.constraint"
    artifactId: "constraint-layout"
    version: "2.0.4"
  }
  digests {
    sha256: "\330\245\341\301\225\006\233\375\\c9\217\371\\&\335\361\364n\365\277=\257\000\"B\a\316\2179\214\336"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "appcompat-v7"
    version: "28.0.0"
  }
  digests {
    sha256: "\243\250\345#\003Ytn\331\030\001W\233_\276Fh\343\261\304\346\241L}g\310\365\214\2601\027R"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-compat"
    version: "28.0.0"
  }
  digests {
    sha256: "\341~;\001\333\352?\236\241\310iC)/\220<\251=\"1\306$.En\vj\234X\027\021\212"
  }
}
library {
  maven_library {
    groupId: "com.android.support.constraint"
    artifactId: "constraint-layout-solver"
    version: "2.0.4"
  }
  digests {
    sha256: " \2241)\215\003\021\v\253L\211\274UtK\251\371o\215\271\203\326Lo\341\331\241\256\276\034{\000"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-annotations"
    version: "28.0.0"
  }
  digests {
    sha256: "][\224\024\360-?\240\356u&\270\325\335\256\r\246|\216\314\214Mc\377\246\317\221H\212\223\271\'"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "collections"
    version: "28.0.0"
  }
  digests {
    sha256: "\223\302X\310\240\237S\032&vS\202\227B\300\370\366\332\0164\213\021\313\206U\260\205V(\362\324\360"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "cursoradapter"
    version: "28.0.0"
  }
  digests {
    sha256: "\207\376\377\347B\270\326,\250\251\203:\276VH8\277jg.1\307\255\023\006\354@\006\255\371\r!"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-core-utils"
    version: "28.0.0"
  }
  digests {
    sha256: "\310\036\036\230\312<\262\355\256\000,i\3175\262*\3546K\214\262\361\004,\227\342\006\353W\220\254A"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-fragment"
    version: "28.0.0"
  }
  digests {
    sha256: "7r\374s\212\332\206\202K\241\244\263\361\227\303\333\326{}\334\376,\235\261\336\225\357.4\207\251\025"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-vector-drawable"
    version: "28.0.0"
  }
  digests {
    sha256: "\366X\230m\226\201r\274\317\355(W\204q\311`Px\017\345\3413\206\036M3\020i\3147?M"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "animated-vector-drawable"
    version: "28.0.0"
  }
  digests {
    sha256: "\'\036\313\311\006\315\250\334\331\346U\272\004s\022\2344\b\244\030\234\200oal7\216o\321\217\263\267"
  }
}
library {
  maven_library {
    groupId: "android.arch.lifecycle"
    artifactId: "runtime"
    version: "1.1.1"
  }
  digests {
    sha256: "\304\344\276f\301\262\360\253\354Y5qEN\035\341@\023\367\340\371k\362\251\362\022\223\032H\312\345P"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "versionedparcelable"
    version: "28.0.0"
  }
  digests {
    sha256: "`\353\034\260\217q\266\\?a#\023^\003\353\353Y0\265\341&\341\345\262\254\221\263\206\220\214\235\002"
  }
}
library {
  maven_library {
    groupId: "android.arch.lifecycle"
    artifactId: "common"
    version: "1.1.1"
  }
  digests {
    sha256: "\2157\216\210\353\325\030\236\t\356\366#AH\022\310h\375\220\252Q\235a`\3421\037\270\270\034\377V"
  }
}
library {
  maven_library {
    groupId: "android.arch.core"
    artifactId: "common"
    version: "1.1.1"
  }
  digests {
    sha256: ":aj2\3643\351\342?Uk8W\\1\260\023a=:\350R\006&;v%\376\037L\025\032"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "documentfile"
    version: "28.0.0"
  }
  digests {
    sha256: "G\315\315>\223\002\267\260d\222?\005Hz\\\003\272\273\331\273\332G&\267\036\227y\037\253]Gy"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "loader"
    version: "28.0.0"
  }
  digests {
    sha256: "\222\v\205\357\327-\303>\221[\017\210\250\203\376s\270\204\203\306\337\207Q\247A\341v\021\362F\003A"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "localbroadcastmanager"
    version: "28.0.0"
  }
  digests {
    sha256: "\322\207\310#\257_\335\347,\t\237\317\305\3660\357\351hz\367\251\0244:\346\375\222\3362\310\250\006"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "print"
    version: "28.0.0"
  }
  digests {
    sha256: "K\350\250\022\327>J\200\343[\221\316\256\022}\357?\v\271rk\363\274C\232\240\314\201P?W("
  }
}
library {
  maven_library {
    groupId: "android.arch.lifecycle"
    artifactId: "livedata"
    version: "1.1.1"
  }
  digests {
    sha256: "P\253\004\220\301\377\032|\373NU@2\231\213\b\b\210\224m\r\324$\363\231\000\357\304\241\274\327P"
  }
}
library {
  maven_library {
    groupId: "android.arch.lifecycle"
    artifactId: "viewmodel"
    version: "1.1.1"
  }
  digests {
    sha256: "}\342\234\372\272w\326\265\325\276#LW\366\201-\001P\320\207\3469A\257\"\272\035\037\216+\311j"
  }
}
library {
  maven_library {
    groupId: "android.arch.core"
    artifactId: "runtime"
    version: "1.1.1"
  }
  digests {
    sha256: "\303!Z\245\2073\021\263\370\212oNJ<%\255\211\227\033\301\'\336\214>\022\221\305\177\223\240\\9"
  }
}
library {
  maven_library {
    groupId: "android.arch.lifecycle"
    artifactId: "livedata-core"
    version: "1.1.1"
  }
  digests {
    sha256: "\326\375\330\271\205\326\027\215~\242\361i\206\242N\203\361\276\3516\267MC\026|i\340\215<\301,P"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "support-core-ui"
    version: "28.0.0"
  }
  digests {
    sha256: "\273\307\366_\311VIFG3\25773aS*\265\371\363\267I\303\272\332\242\273\362~WKlo"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "customview"
    version: "28.0.0"
  }
  digests {
    sha256: "\230\333\003\204_\231N\b$\213\367\001\301\377\f\312\241.p\371BQ\354\222r\220\017\017iN\a+"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "viewpager"
    version: "28.0.0"
  }
  digests {
    sha256: "\001<LS\005\207X\354\020M\272\351p\276X\025\237u\337\343B\272\213\223}\025\377R\202\343_\374"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "coordinatorlayout"
    version: "28.0.0"
  }
  digests {
    sha256: "\235\372\315\200B=\311y\004\217\272\355\203\300\356T<F%\237\353$\0277~y\246V\210\2158\222"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "drawerlayout"
    version: "28.0.0"
  }
  digests {
    sha256: "\217h\t\257\256G\223U\f7F\034\230\020\351T\256j#\333\264\322>S3\277\030\024\215\361\025\n"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "slidingpanelayout"
    version: "28.0.0"
  }
  digests {
    sha256: "\321\3224\366j\0336\251\256\351\271O\246\306o\227\022\214\b(\a\214\216\210\236\2207\354\211\214\326\000"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "interpolator"
    version: "28.0.0"
  }
  digests {
    sha256: "{\307\356\206\240\3339\244\265\031V\363\350\230B\322\275\226!\030\325}w\236\266\355k4\272\006w\352"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "swiperefreshlayout"
    version: "28.0.0"
  }
  digests {
    sha256: "\243\264\037\177g0\206kI\206^\206\344\237\230\215HXi\227e\36540\017\262\377_\223%\347\022"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "asynclayoutinflater"
    version: "28.0.0"
  }
  digests {
    sha256: "\021[\336\207r\037s4W\233\fs_`\335|\230\257\033\267\363@\020\305\260U;\225\3345\032\242"
  }
}
library {
  maven_library {
    groupId: "android.arch.navigation"
    artifactId: "navigation-fragment"
    version: "1.0.0"
  }
  digests {
    sha256: "\343\242\352y\f\002\354U\212\271\273\263w\001\271\327\260\212f\210\233\367\004\326\320\213\t\246 \022=\341"
  }
}
library {
  maven_library {
    groupId: "android.arch.navigation"
    artifactId: "navigation-runtime"
    version: "1.0.0"
  }
  digests {
    sha256: "\361\254\232\366\253\216\227\235\341&\025\336\303\355\224\020y\301\200>w\321\034\315\343pY\004\017Bb-"
  }
}
library {
  maven_library {
    groupId: "android.arch.navigation"
    artifactId: "navigation-common"
    version: "1.0.0"
  }
  digests {
    sha256: "\030\325\235\032f\305\002m\322C\234\377\252\305\vE!%\252\203M\316P\033\215o\202\355\032\275\204?"
  }
}
library {
  maven_library {
    groupId: "android.arch.navigation"
    artifactId: "navigation-ui"
    version: "1.0.0"
  }
  digests {
    sha256: "\352\2237\366a\263L\367o%\001\212\275\353\370\252\240\314\350\235\v43\027f\003\327F\n\363\nI"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "design"
    version: "28.0.0"
  }
  digests {
    sha256: "xt\255\031\004\356\334t\252A\317\377\373\177u\235\211\220\005o;\273\311&I\021e\034g\304/_"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "transition"
    version: "28.0.0"
  }
  digests {
    sha256: "E\320\237\305\022\204\301{\272\263\000\365\022%\022\254}sH\246\322;\332 Qd\213\276v\314\232\245"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "cardview-v7"
    version: "28.0.0"
  }
  digests {
    sha256: "\274\236k\016\006\316\022\005\361\3334\360\346\0310\031a=\031\317\353T\315\314\352r#@\321\306\017&"
  }
}
library {
  maven_library {
    groupId: "com.android.support"
    artifactId: "recyclerview-v7"
    version: "28.0.0"
  }
  digests {
    sha256: "\353)d\024\301\366\324\307\265\"\366\237\345\005\210\352\205)xU\333\016x\006\302N\264\367T\tX}"
  }
}
library {
  maven_library {
    groupId: "com.jakewharton"
    artifactId: "butterknife"
    version: "8.2.0"
  }
  digests {
    sha256: "\346\327QA\336c\344E\217?\255N:UV\224\032\357\1776\312\340\346\374\023\203w\362\315\337g\216"
  }
}
library {
  maven_library {
    groupId: "com.jakewharton"
    artifactId: "butterknife-annotations"
    version: "8.2.0"
  }
  digests {
    sha256: "`q\v\022\r\375\207\351\303\352Z\030\217\350\270(\217\336\305P\001\250\354\037P\250)>\274xx\222"
  }
}
library {
  maven_library {
    groupId: "com.orhanobut"
    artifactId: "logger"
    version: "1.15"
  }
  digests {
    sha256: "\000Fw|\367\034\222\307Q\361\003\372N\366p\326\345U|a\374\230J\340\245\025\217\305\254\331\022\314"
  }
}
library {
  maven_library {
    groupId: "io.jsonwebtoken"
    artifactId: "jjwt"
    version: "0.7.0"
  }
  digests {
    sha256: "+\\\032\213\306T\300ad\'\337\257\360\302*\256\266\305\273\t\240\332<\372?\353\032\204\254\005\347\212"
  }
}
library {
  maven_library {
    groupId: "com.fasterxml.jackson.core"
    artifactId: "jackson-databind"
    version: "2.8.2"
  }
  digests {
    sha256: "\242\251\004\231e}\177g\301y3\206\235\357wE\360\025YDF\000s\322\361\265Y\b\004\205\225v"
  }
}
library {
  maven_library {
    groupId: "com.fasterxml.jackson.core"
    artifactId: "jackson-annotations"
    version: "2.8.0"
  }
  digests {
    sha256: "\346\033sC\254\356\266\354\332)\035N\3613\315>v_\027\214c\0345\177\375\b\032\272\267\361]\270"
  }
}
library {
  maven_library {
    groupId: "com.fasterxml.jackson.core"
    artifactId: "jackson-core"
    version: "2.8.2"
  }
  digests {
    sha256: "N\375\016\005\354|F\210v\237a-\315\331&NV\225\311;\210)\\\336\326\274\224\320\271Rl?"
  }
}
library {
  maven_library {
    groupId: "commons-io"
    artifactId: "commons-io"
    version: "2.4"
  }
  digests {
    sha256: "\314jA\334>\252\314\236D\nk\320\322\211\v \323kN\344\b\376-g\022/2\213\266\340\025\201"
  }
}
library {
  digests {
    sha256: "\337\240\034)?\336\302)\210!\315\030\310\306\020+9(\375,7\341\345\233\vRD&,\317\307\323"
  }
}
library {
  digests {
    sha256: "(\273wN\021u\b\222v\301\310\341\263\003\361\241\324\240\271E\323m\'\362v#\246\232\305[\254\331"
  }
}
library {
  digests {
    sha256: "\b\251\233\205.\257\251\b\367k\037L\207#\304|3\231\313\334h+\251\020juV\361\300k\361g"
  }
}
library_dependencies {
  library_dep_index: 1
  library_dep_index: 2
  library_dep_index: 3
}
library_dependencies {
  library_index: 1
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 5
  library_dep_index: 6
  library_dep_index: 7
  library_dep_index: 8
  library_dep_index: 9
  library_dep_index: 10
}
library_dependencies {
  library_index: 4
}
library_dependencies {
  library_index: 2
  library_dep_index: 4
  library_dep_index: 5
  library_dep_index: 11
  library_dep_index: 12
}
library_dependencies {
  library_index: 5
  library_dep_index: 4
}
library_dependencies {
  library_index: 11
  library_dep_index: 13
  library_dep_index: 14
  library_dep_index: 4
}
library_dependencies {
  library_index: 13
  library_dep_index: 4
}
library_dependencies {
  library_index: 14
  library_dep_index: 4
}
library_dependencies {
  library_index: 12
  library_dep_index: 4
  library_dep_index: 5
}
library_dependencies {
  library_index: 6
  library_dep_index: 4
}
library_dependencies {
  library_index: 7
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 15
  library_dep_index: 16
  library_dep_index: 17
  library_dep_index: 18
}
library_dependencies {
  library_index: 15
  library_dep_index: 4
}
library_dependencies {
  library_index: 16
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 19
  library_dep_index: 20
}
library_dependencies {
  library_index: 19
  library_dep_index: 21
  library_dep_index: 22
  library_dep_index: 14
}
library_dependencies {
  library_index: 21
  library_dep_index: 4
  library_dep_index: 14
}
library_dependencies {
  library_index: 22
  library_dep_index: 13
  library_dep_index: 14
  library_dep_index: 21
}
library_dependencies {
  library_index: 20
  library_dep_index: 4
}
library_dependencies {
  library_index: 17
  library_dep_index: 4
}
library_dependencies {
  library_index: 18
  library_dep_index: 4
}
library_dependencies {
  library_index: 8
  library_dep_index: 2
  library_dep_index: 23
  library_dep_index: 7
  library_dep_index: 4
  library_dep_index: 16
  library_dep_index: 20
}
library_dependencies {
  library_index: 23
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 7
  library_dep_index: 24
  library_dep_index: 25
  library_dep_index: 26
  library_dep_index: 27
  library_dep_index: 28
  library_dep_index: 29
  library_dep_index: 30
  library_dep_index: 31
  library_dep_index: 6
}
library_dependencies {
  library_index: 24
  library_dep_index: 4
  library_dep_index: 2
}
library_dependencies {
  library_index: 25
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 24
}
library_dependencies {
  library_index: 26
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 24
}
library_dependencies {
  library_index: 27
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 24
}
library_dependencies {
  library_index: 28
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 24
}
library_dependencies {
  library_index: 29
  library_dep_index: 4
}
library_dependencies {
  library_index: 30
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 29
}
library_dependencies {
  library_index: 31
  library_dep_index: 4
  library_dep_index: 2
}
library_dependencies {
  library_index: 9
  library_dep_index: 4
  library_dep_index: 2
}
library_dependencies {
  library_index: 10
  library_dep_index: 9
  library_dep_index: 23
}
library_dependencies {
  library_index: 3
}
library_dependencies {
  library_index: 32
  library_dep_index: 8
  library_dep_index: 33
}
library_dependencies {
  library_index: 33
  library_dep_index: 7
  library_dep_index: 34
}
library_dependencies {
  library_index: 34
  library_dep_index: 2
}
library_dependencies {
  library_index: 35
  library_dep_index: 33
  library_dep_index: 36
}
library_dependencies {
  library_index: 36
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 23
  library_dep_index: 7
  library_dep_index: 8
  library_dep_index: 37
  library_dep_index: 1
  library_dep_index: 38
  library_dep_index: 39
}
library_dependencies {
  library_index: 37
  library_dep_index: 4
  library_dep_index: 2
}
library_dependencies {
  library_index: 38
  library_dep_index: 4
}
library_dependencies {
  library_index: 39
  library_dep_index: 4
  library_dep_index: 2
  library_dep_index: 23
}
library_dependencies {
  library_index: 40
  library_dep_index: 41
  library_dep_index: 4
}
library_dependencies {
  library_index: 41
  library_dep_index: 4
}
library_dependencies {
  library_index: 42
}
library_dependencies {
  library_index: 43
  library_dep_index: 44
}
library_dependencies {
  library_index: 44
  library_dep_index: 45
  library_dep_index: 46
}
library_dependencies {
  library_index: 45
}
library_dependencies {
  library_index: 46
}
library_dependencies {
  library_index: 47
}
library_dependencies {
  library_index: 48
}
library_dependencies {
  library_index: 49
}
library_dependencies {
  library_index: 50
}
module_dependencies {
  module_name: "base"
  dependency_index: 0
  dependency_index: 32
  dependency_index: 1
  dependency_index: 35
  dependency_index: 36
  dependency_index: 39
  dependency_index: 40
  dependency_index: 42
  dependency_index: 43
  dependency_index: 47
  dependency_index: 48
  dependency_index: 49
  dependency_index: 50
}
