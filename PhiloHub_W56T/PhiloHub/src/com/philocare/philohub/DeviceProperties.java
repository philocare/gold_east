package com.philocare.philohub;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import com.sxr.sdk.ble.keepfit.aidl.AlarmInfoItem;
import com.sxr.sdk.ble.keepfit.aidl.BleClientOption;
import com.sxr.sdk.ble.keepfit.aidl.DeviceProfile;
import com.sxr.sdk.ble.keepfit.aidl.IRemoteService;
import com.sxr.sdk.ble.keepfit.aidl.IServiceCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

public class DeviceProperties {

    private static final String TAG = MainActivity.class.getSimpleName();
    private IRemoteService mService;
    private boolean bOpen = false;
    private boolean bSave = true;
    private Context mCtx = null;
    private String pathLog = "/philocare/log/";

    protected String curMac;

    private int sleepcount = 0;

    private ArrayList<BleDeviceItem> nearbyItemList;

    private boolean bColor = false;
    private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

    public DeviceProperties(Context ctx, IBinder service) {
        try {
            mCtx = ctx;
            mService = IRemoteService.Stub.asInterface(service);
            mService.openSDKLog(bSave, pathLog, "blue.log");
            boolean isConnected = callRemoteIsConnected();

            if (isConnected == false) {
                // disconectou
                //llConnect.setVisibility(View.GONE);
            } else {
                // pulseira autorizada
                int authrize = callRemoteIsAuthrize();
                if (authrize == 200) {
                    // se está autorizado, conecta na pulseira
                    String curMac = callRemoteGetConnectedDevice();
                    //llConnect.setVisibility(View.VISIBLE);
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        callRemoteDisconnect();
    }

    public void onDestroy() {
        callRemoteDisconnect();
    }

    private String callRemoteGetConnectedDevice() {
        String deviceMac = "";
        if (mService != null) {
            try {
                deviceMac = mService.getConnectedDevice();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }

        return deviceMac;
    }


    private int callRemoteIsAuthrize() {
        int isAuthrize = 0;
        if (mService != null) {
            try {
                isAuthrize = mService.isAuthrize();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }

        return isAuthrize;
    }

    private void callRemoteDisconnect() {

        if (mService != null) {
            try {
                mService.disconnectBt(true);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean callRemoteIsConnected() {
        boolean isConnected = false;
        if (mService != null) {
            try {
                isConnected = mService.isConnectBt();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }

        return isConnected;
    }


    Handler updateConnectStateHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            //super.handleMessage(msg);
            Bundle data = msg.getData();
            int state = data.getInt("state");

            if (state == 2) {
                //txtDeviceConnected.setText("Dispositivo conectado");
                //llConnect.setVisibility(View.VISIBLE);
            } else {
                //txtDeviceConnected.setText("Dispositivo desconectado");
                //llConnect.setVisibility(View.GONE);
            }
            return true;
        }
    });

    protected void updateConnectState(int state) {
        Message msg = new Message();
        Bundle data = new Bundle();
        data.putInt("state", state);
        msg.setData(data);
        updateConnectStateHandler.sendMessage(msg);
        // Fazer botões desaparecerem

    }

    private void callRemoteConnect(String name, String mac) {
        if (mac == null || mac.length() == 0) {
            Toast.makeText(mCtx, "ble device mac address is not correctly!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mService != null) {
            try {
                mService.connectBt(name, mac);
                // desliga a tela de scan
                //dismissPopWindow();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    Handler scanDeviceHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            //super.handleMessage(msg);
            Bundle data = msg.getData();
            String result = data.getString("result");
            //nearbyListAdapter.notifyDataSetChanged();
            return true;
        }
    });

    private IServiceCallback mServiceCallback = new IServiceCallback.Stub() {
        @Override
        public void onConnectStateChanged(int state) throws RemoteException {
            showToast("onConnectStateChanged", curMac + " state " + state);
            updateConnectState(state);
        }


        @Override
        public void onScanCallback(String deviceName, String deviceMacAddress, int rssi)
                throws RemoteException {
            Log.i(TAG, String.format("onScanCallback <%1$s>[%2$s](%3$d)", deviceName, deviceMacAddress, rssi));

            if(nearbyItemList == null)
                return;

            // Modificação para encontrar sempre a pulseira cadastrada
            // Atual: "A4:F6:3D:B8:FB:8C"

            Iterator<BleDeviceItem> iter = nearbyItemList.iterator();
            BleDeviceItem item = null;
            boolean bExist = false;
            while (iter.hasNext()) {

                item = (BleDeviceItem) iter.next();
                if (item.getBleDeviceAddress().equalsIgnoreCase(deviceMacAddress) == true) {
                    bExist = true;
                    // mostra a proximidade da pulseira
                    item.setRssi(rssi);
                    if (rssi > -90) {
                        // desliga a tela de scan
                        //dismissPopWindow();
                        // conecta a pulseira
                        callRemoteConnect(deviceName, deviceMacAddress);
                        //mService.connectBt(deviceName, deviceMacAddress);
                    }
                    break;
                }
            }
            // Aqui começa a alteração para detectar apenas a pulseira de interesse
            if ((!bExist) && (deviceMacAddress.equalsIgnoreCase("A4:F6:3D:B8:FB:8C"))) {
                item = new BleDeviceItem(deviceName, deviceMacAddress, "", "", rssi, "");
                nearbyItemList.add(item);
                //Collections.sort(nearbyItemList, new ComparatorBleDeviceItem());
            }

            Message msg = new Message();
            scanDeviceHandler.sendMessage(msg);


//            if(deviceName.equals("S7-0192")){
//                mService.connectBt(deviceName, deviceMacAddress);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            mService.disconnectBt(true);
//                            mService.scanDevice(true);
//                        } catch (RemoteException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, 5000);
//            }
        }


        @Override
        public void onSetNotify(int result) throws RemoteException {
            showToast("onSetNotify", String.valueOf(result));
        }

        @Override
        public void onSetUserInfo(int result) throws RemoteException {
            showToast("onSetUserInfo", "" + result);
        }

        @Override
        public void onAuthSdkResult(int errorCode) throws RemoteException {
            showToast("onAuthSdkResult", errorCode + "");
            if (errorCode == 200) {
                // habilita o scan se conectou no serviço de bluetooth
                //callRemoteScanDevice();

            }
        }

        /*private void callRemoteScanDevice() {
            // Exibe splash window
            //popSplashWindow(findViewById(R.id.main_layout), R.layout.activity_splash_old);

            if (nearbyItemList != null)
                nearbyItemList.clear();

            if (mService != null) {
                try {
                    nearbyItemList = new ArrayList<BleDeviceItem>();
                    bStart = !bStart;
                    mService.scanDevice(bStart);
                } catch (RemoteException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Remote call error!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Service is not available yet!", Toast.LENGTH_SHORT).show();
            }
        }*/

        @Override
        public void onGetDeviceTime(int result, String time) throws RemoteException {
            showToast("onGetDeviceTime", String.valueOf(time));
        }

        @Override
        public void onSetDeviceTime(int arg0) throws RemoteException {
            showToast("onSetDeviceTime", arg0 + "");
        }

        @Override
        public void onSetDeviceInfo(int arg0) throws RemoteException {
            showToast("onSetDeviceInfo", arg0 + "");
        }


        @Override
        public void onAuthDeviceResult(int arg0) throws RemoteException {
            showToast("onAuthDeviceResult", arg0 + "");
        }


        @Override
        public void onSetAlarm(int arg0) throws RemoteException {
            showToast("onSetAlarm", arg0 + "");
        }

        @Override
        public void onSendVibrationSignal(int arg0) throws RemoteException {
            showToast("onSendVibrationSignal", "result:" + arg0);
        }

        @Override
        public void onGetDeviceBatery(int arg0, int arg1)
                throws RemoteException {
            showToast("onGetDeviceBatery", "batery:" + arg0 + ", statu " + arg1);
        }


        @Override
        public void onSetDeviceMode(int arg0) throws RemoteException {
            showToast("onSetDeviceMode", "result:" + arg0);
        }

        @Override
        public void onSetHourFormat(int arg0) throws RemoteException {
            showToast("onSetHourFormat ", "result:" + arg0);

        }

        @Override
        public void setAutoHeartMode(int arg0) throws RemoteException {
            showToast("setAutoHeartMode ", "result:" + arg0);
        }


        @Override
        public void onGetCurSportData(int type, long timestamp, int step, int distance,
                                      int cal, int cursleeptime, int totalrunningtime, int steptime) throws RemoteException {
            Date date = new Date(timestamp * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String time = sdf.format(date);
            showToast("onGetCurSportData", "type : " + type + " , time :" + time + " , step: " + step + ", distance :" + distance + ", cal :" + cal + ", cursleeptime :" + cursleeptime + ", totalrunningtime:" + totalrunningtime);


        }

        @Override
        public void onGetSenserData(int result, long timestamp, int heartrate, int sleepstatu)
                throws RemoteException {
            Date date = new Date(timestamp * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String time = sdf.format(date);
            showToast("onGetSenserData", "result: " + result + ",time:" + time + ",heartrate:" + heartrate + ",sleepstatu:" + sleepstatu);

        }


        @Override
        public void onGetDataByDay(int type, long timestamp, int step, int heartrate)
                throws RemoteException {
            Date date = new Date(timestamp * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String recorddate = sdf.format(date);
            showToast("onGetDataByDay", "type:" + type + ",time::" + recorddate + ",step:" + step + ",heartrate:" + heartrate);
            if (type == 2) {
                sleepcount++;
            }
        }

        @Override
        public void onGetDataByDayEnd(int type, long timestamp) throws RemoteException {
            Date date = new Date(timestamp * 1000);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            String recorddate = sdf.format(date);
            showToast("onGetDataByDayEnd", "time:" + recorddate + ",sleepcount:" + sleepcount);
            sleepcount = 0;
        }


        @Override
        public void onSetPhontMode(int arg0) throws RemoteException {
            showToast("onSetPhontMode", "result:" + arg0);
        }


        @Override
        public void onSetSleepTime(int arg0) throws RemoteException {
            showToast("onSetSleepTime", "result:" + arg0);
        }


        @Override
        public void onSetIdleTime(int arg0) throws RemoteException {
            showToast("onSetIdleTime", "result:" + arg0);
        }


        @Override
        public void onGetDeviceInfo(int version, String macaddress, String vendorCode,
                                    String productCode, int result) throws RemoteException {
            showToast("onGetDeviceInfo", "version :" + version + ",macaddress : " + macaddress + ",vendorCode : " + vendorCode + ",productCode :" + productCode + " , CRCresult :" + result);

        }

        @Override
        public void onGetDeviceAction(int type) throws RemoteException {
            showToast("onGetDeviceAction", "type:" + type);
        }


        @Override
        public void onGetBandFunction(int result, boolean[] results) throws RemoteException {
            showToast("onGetBandFunction", "result : " + result + ", results :" + results.length);

            String function = "";
            for(int i = 0; i < results.length; i ++){
                function += String.valueOf((i+1) + "=" + results[i] + " ");
            }
            showToast("onGetBandFunction", function);
        }

        @Override
        public void onSetLanguage(int arg0) throws RemoteException {
            showToast("onSetLanguage", "result:" + arg0);
        }


        @Override
        public void onSendWeather(int arg0) throws RemoteException {
            showToast("onSendWeather", "result:" + arg0);
        }


        @Override
        public void onSetAntiLost(int arg0) throws RemoteException {
            showToast("onSetAntiLost", "result:" + arg0);

        }


        @Override
        public void onReceiveSensorData(int arg0, int arg1, int arg2, int arg3,
                                        int arg4) throws RemoteException {
            showToast("onReceiveSensorData", "result:" + arg0 + " , " + arg1 + " , " + arg2 + " , " + arg3 + " , " + arg4);
        }


        @Override
        public void onSetBloodPressureMode(int arg0) throws RemoteException {
            showToast("onSetBloodPressureMode", "result:" + arg0);
        }


        @Override
        public void onGetMultipleSportData(int flag, String recorddate, int mode, int value)
                throws RemoteException {
//            Date date = new Date(timestamp * 1000);
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
//            String recorddate = sdf.format(date);
            showToast("onGetMultipleSportData", "flag:" + flag + " , mode :" + mode + " recorddate:" + recorddate + " , value :" + value);
        }


        @Override
        public void onSetGoalStep(int result) throws RemoteException {
            showToast("onSetGoalStep", "result:" + result);
        }


        @Override
        public void onSetDeviceHeartRateArea(int result) throws RemoteException {
            showToast("onSetDeviceHeartRateArea", "result:" + result);
        }


        @Override
        public void onSensorStateChange(int type, int state)
                throws RemoteException {

            showToast("onSensorStateChange", "type:" + type + " , state : " + state);
        }

        @Override
        public void onReadCurrentSportData(int mode, String time, int step,
                                           int cal) throws RemoteException {

            showToast("onReadCurrentSportData", "mode:" + mode + " , time : " + time + " , step : " + step + " cal :" + cal);
        }

        @Override
        public void onGetOtaInfo(boolean isUpdate, String version, String path) throws RemoteException {
            showToast("onGetOtaInfo", "isUpdate " + isUpdate + " version " + version + " path " + path);
        }

        @Override
        public void onGetOtaUpdate(int step, int progress) throws RemoteException {
            showToast("onGetOtaUpdate", "step " + step + " progress " + progress);
        }

        @Override
        public void onSetDeviceCode(int result) throws RemoteException {
            showToast("onSetDeviceCode", "result " + result);
        }

        @Override
        public void onGetDeviceCode(byte[] bytes) throws RemoteException {
            showToast("onGetDeviceCode", "bytes " + SysUtils.printHexString(bytes));
        }

        @Override
        public void onCharacteristicChanged(String uuid, byte[] bytes) throws RemoteException {
            showToast("onCharacteristicChanged", uuid + " " + SysUtils.printHexString(bytes));
        }

        @Override
        public void onCharacteristicWrite(String uuid, byte[] bytes, int status) throws RemoteException {
            showToast("onCharacteristicWrite", status + " " + uuid + " " + SysUtils.printHexString(bytes));
        }
    };


    private Handler messageHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Bundle data = msg.getData();
            String title = data.getString("title");
            String content = data.getString("content");

            Log.i(TAG, title + ": " + content);
            switch (title) {
                default:
                    String text = "[" + sdf.format(new Date()) + "] " + title + "\n" + content;
                    //data_text.setText(text);
                    text = "<font color='" + (bColor ? "#00" : "#82") + "'>" + text.replace("\n", "<br>") + "</font><br>";
                    //tvSync.append(Html.fromHtml(text));
                    //svLog.fullScroll(View.FOCUS_DOWN);
                    bColor = !bColor;
                    break;
            }
            return true;
        }
    });

    protected void showToast(String title, String content) {
        String file = "demo.log";
        //if(bSave)
        //    SysUtils.writeTxtToFile(title + " -> " + content, pathLog, file);
        Message msg = new Message();
        Bundle data = new Bundle();
        data.putString("title", title);
        data.putString("content", content);
        msg.setData(data);
        messageHandler.sendMessage(msg);
    }
    
    public void setUuid() {
        if (mService != null) {
            try {
                bOpen = !bOpen;
                mService.setUuid(new String[0], new String[0], bOpen);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }


    public void callGetDeviceCode() {
        if (mService != null) {
            try {
                mService.getDeviceCode();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callSetDeviceCode(byte[] bytes) {
        if (mService != null) {
            try {
                showToast("callSetDeviceCode", SysUtils.printHexString(bytes));
                mService.setDeviceCode(bytes);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callSetParameters() {
        int result;
        if (mService != null) {
            try {
                DeviceProfile deviceProfile = new DeviceProfile(true, true, false, 1, 2, 00, 00);
                BleClientOption opt2 = new BleClientOption(null, deviceProfile, null);
                int result2 = callRemoteSetOption(opt2);
                result = mService.setDeviceInfo();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    private int callRemoteSetOption(BleClientOption opt) {
        int result = 0;
        if (mService != null) {
            try {
                result = mService.setOption(opt);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }

        return result;
    }

    public void callSetAlarm() {
        boolean result;
        if (mService != null) {
            try {
                ArrayList<AlarmInfoItem> lAlarmInfo = new ArrayList<AlarmInfoItem>();
                AlarmInfoItem item = new AlarmInfoItem(1, 1, 1, 11, 1, 1, 1, 1, 1, 1, 1, "要睡觉le", false);
                lAlarmInfo.add(item);
                BleClientOption bco = new BleClientOption(null, null, lAlarmInfo);
                mService.setOption(bco);
                mService.setAlarm();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callNotify() {
        boolean result;
        if (mService != null) {
            /*try {
                //String type = ((EditText) findViewById(R.id.etType)).getText().toString();
                //String name = ((EditText) findViewById(R.id.etName)).getText().toString();
                //String content = ((EditText) findViewById(R.id.etContent)).getText().toString();
                //result = mService.setNotify(System.currentTimeMillis() + "", Integer.parseInt(type), name, content);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }*/
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callGetOtaInfo() {
        int result;
        if (mService != null) {
            try {
                result = mService.getOtaInfo(true);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callSetDeviceTime() {
        int result;
        if (mService != null) {
            try {
                result = mService.setDeviceTime();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callgetCurSportData() {
        int result;
        if (mService != null) {
            try {
                result = mService.getCurSportData();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callSet_vir() {
        int result;
        if (mService != null) {
            try {
                result = mService.sendVibrationSignal(4); //震动4次
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetphoto() {
        int result;
        if (mService != null) {
            try {
                result = mService.setPhontMode(true);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetIdletime() {
        int result;
        if (mService != null) {
            try {
                result = mService.setIdleTime(300, 14, 00, 18, 00);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetSleepTime() {
        int result;
        if (mService != null) {
            try {
                result = mService.setSleepTime(12, 00, 14, 00, 22, 00, 8, 00);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteGetDeviceBatery() {
        int result;
        if (mService != null) {
            try {
                result = mService.getDeviceBatery();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteGetDeviceInfo() {
        int result;
        if (mService != null) {
            try {
                result = mService.getDeviceInfo();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetDeviceMode() {
        int result;
        if (mService != null) {
            try {
                result = mService.setDeviceMode(3);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetHourFormat() {
        int result;
        if (mService != null) {
            try {
                boolean is24HourFormat = DateFormat.is24HourFormat(mCtx);
                result = mService.setHourFormat(is24HourFormat == true ? 0 : 1);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetHeartRateMode(boolean enable) {
        int result;
        if (mService != null) {
            try {
                result = mService.setHeartRateMode(enable, 60);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetAutoHeartMode(boolean enable) {
        int result;
        if (mService != null) {
            try {
                result = mService.setAutoHeartMode(enable, 18, 00, 19, 00, 15, 2); //18:00 - 19:00  15min 2min
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteGetData(int type, int day) {
        Log.i(TAG, "callRemoteGetData");
        int result;
        if (mService != null) {
            try {
                result = mService.getDataByDay(type, day);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteGetFunction() {
        Log.i(TAG, "callRemoteGetFunction");
        int result;
        if (mService != null) {
            try {
                result = mService.getBandFunction();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetLanguage() {
        Log.i(TAG, "callRemoteGetData");
        int result;
        if (mService != null) {
            try {
                result = mService.setLanguage();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetWeather() {
        Log.i(TAG, "callRemoteGetData");
        int result;
        if (mService != null) {
            try {
                result = mService.sendWeather();
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteGetMutipleData(int day) {
        Log.i(TAG, "callRemoteGetMutipleData");
        int result;
        if (mService != null) {
            try {
                result = mService.getMultipleSportData(day);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteOpenBlood(boolean enable) {
        Log.i(TAG, "callRemoteGetMutipleData");
        int result;
        if (mService != null) {
            try {
                result = mService.setBloodPressureMode(enable);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetGoalStep(int step) {
        Log.i(TAG, "callRemoteOpenBlood");
        int result;
        if (mService != null) {
            try {
                result = mService.setGoalStep(step);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

    public void callRemoteSetHeartRateArea(boolean enable, int max, int min) {
        Log.i(TAG, "callRemoteOpenBlood");
        int result;
        if (mService != null) {
            try {
                result = mService.setDeviceHeartRateArea(enable, max, min);
            } catch (RemoteException e) {
                e.printStackTrace();
                Toast.makeText(mCtx, "Remote call error!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(mCtx, "Service is not available yet!", Toast.LENGTH_SHORT).show();
        }
    }

}
