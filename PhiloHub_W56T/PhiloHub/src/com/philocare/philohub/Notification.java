package com.philocare.philohub;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.sxr.sdk.ble.keepfit.client.R;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;
import static com.philocare.philohub.MainActivity.ACTION_SNOOZE;
import static com.philocare.philohub.MainActivity.CHANNEL_ID;

public class Notification extends Activity {


    private Context ctx;
    private Class classAct;
    private NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManager;

    public Notification(Context mCtx, Class classAct){
        this.ctx = mCtx;
        this.classAct  = classAct;
    }

    public void send_note(String bannerTxt, String title, String txt, int icon, PendingIntent act){

        Intent tapIntent = new Intent(ctx, ReportActivity.class);
        tapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //snoozeIntent.setAction(MainActivity.ACTION_SNOOZE);
        //snoozeIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent tapPendingIntent = PendingIntent.getActivity(ctx, 0, tapIntent, 0);

        /*Intent snoozeIntent = new Intent(ctx, BcastReceiver.class);
        snoozeIntent.setAction(ACTION_SNOOZE);
        snoozeIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent snoozePendingIntent =
                PendingIntent.getBroadcast(ctx, 0, snoozeIntent, 0);*/

        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle("textTitle")
                .setContentText("textContent")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);*/

        // -- Funcionando perfeitamente
        builder = new NotificationCompat.Builder(ctx, CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(bannerTxt)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(txt))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(tapPendingIntent)
                .setAutoCancel(true);

        // com Botão de ação
        /*builder = new NotificationCompat.Builder(ctx, CHANNEL_ID)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(bannerTxt)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(txt))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(tapPendingIntent)
                .setAutoCancel(true)
                .addAction(R.mipmap.ic_launcher_round,
                        getString(R.string.snooze),
                        act);*/

        notificationManager = NotificationManagerCompat.from(ctx);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(1, builder.build());

    }

}
